//
//  CalendarViewController.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 25/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import CLWeeklyCalendarView
import ObjectMapper
import Alamofire

class CalendarViewController: UIViewController {

    @IBOutlet weak var calendarView: CLWeeklyCalendarView!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var listTable: UITableView!
    @IBOutlet weak var overallView: UIView!
    @IBOutlet weak var buttonBlockCalendar: UIButton!
    @IBOutlet weak var buttonAddAppointment: UIButton!
    var calenderdetail : calendermodel?
    var selectedDate : String = ""
    
    private lazy var loader  : UIView = {
        return createActivityIndicator(self.view.window!)
    }()
    
    var calendarData : calenderAvailableModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialLoads()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hideTabBar()
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
          self.getAppointmentDetail(date: self.calendarView.selectedDate.description)
        }
    }
    
}

extension CalendarViewController {
    func initialLoads() {
        
        setCalendarView()
        registerCell()
        self.localize()
        
        self.buttonCancel.addTarget(self, action: #selector(buttonCancelAction), for: .touchUpInside)
        self.buttonAddAppointment.addTarget(self, action: #selector(AddAppointment), for: .touchUpInside)
        self.buttonBlockCalendar.addTarget(self, action: #selector(blockCalendar), for: .touchUpInside)
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        self.roundCorners(cornerRadius: 10, cornerView: self.overallView)
    }
    
    private func localize(){
        self.labelTitle.text = Constants.string.calendar.localize()
        self.buttonAddAppointment.setTitle(Constants.string.addAppointment.localize(), for: .normal)
//        self.buttonCancel.setTitle(Constants.string.Cancel.localize(), for: .normal)
    }
    
    func roundCorners(cornerRadius: Double,cornerView:UIView) {
        cornerView.layer.cornerRadius = CGFloat(cornerRadius)
        cornerView.clipsToBounds = true
        cornerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    @IBAction func buttonCancelAction() {
        self.backButtonClick()
    }
    
    @IBAction func blockCalendar() {
        
        self.push(id: Storyboard.Ids.BlockCalendarViewController, animation: true)
    }
    
    @IBAction func AddAppointment() {
   
       let view = TimePickerAlert.getView
       view.alertdelegate = self
       AlertBuilder().addView(fromVC: self , view).show()
   
      
     
//        self.push(id: Storyboard.Ids.AddAppointmentTableViewController, animation: true)
    }
    
    func registerCell() {
        
        self.listTable.register(UINib(nibName: XIB.Names.CalendarTableViewCell, bundle: .main), forCellReuseIdentifier: XIB.Names.CalendarTableViewCell)
    }
    
    func setCalendarView() {
        self.calendarView.calendarAttributes = [CLCalendarBackgroundImageColor : UIColor.clear,

        //Unselected days in the past and future, colour of the text and background.
            CLCalendarPastDayNumberTextColor : UIColor(named: "TextBlackColor")!,
            CLCalendarFutureDayNumberTextColor : UIColor(named: "TextBlackColor")!,

            CLCalendarCurrentDayNumberTextColor : UIColor(named: "TextBlackColor")!,
            CLCalendarCurrentDayNumberBackgroundColor : UIColor.clear,

        //Selected day (either today or non-today)
            CLCalendarSelectedDayNumberTextColor : UIColor(named: "TextBlackColor")!,
            CLCalendarSelectedDayNumberBackgroundColor : UIColor(named: "CalenderColorSet")!,
            CLCalendarSelectedCurrentDayNumberTextColor : UIColor(named: "TextBlackColor")!,
            CLCalendarSelectedCurrentDayNumberBackgroundColor : UIColor(named: "CalenderColorSet")!,

        //Day: e.g. Saturday, 1 Dec 2016
            CLCalendarDayTitleTextColor : UIColor(named: "TextBlackColor")!,
            CLCalendarSelectedDatePrintColor : UIColor(named: "TextBlackColor")!]
        self.calendarView.delegate = self

    }
    
     func alertAction(appointment : Doctor_request){
        let alert = UIAlertController(title: "Appointment Options", message: nil, preferredStyle: .actionSheet)



        alert.addAction(UIAlertAction(title: "Cancel Appointment", style: .default, handler: { (_) in
            self.cancelAppointmentDetail(id: appointment.id?.description ?? "0")

        }))

        alert.addAction(UIAlertAction(title: "Call Patient", style: .default, handler: { (_) in
            if let url = URL(string: "tel://\(appointment.patient?.patient_contact_number ?? "")"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }

        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (_) in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    
  

}


extension CalendarViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.calendarData?.responseData?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.listTable.dequeueReusableCell(withIdentifier: XIB.Names.CalendarTableViewCell, for: indexPath) as! CalendarTableViewCell
        cell.buttonOptions.addTap {
            self.alertAction(appointment: self.calendarData?.responseData?[indexPath.row] ?? Doctor_request())
        }
        self.setupData(cell: cell, data: self.calendarData?.responseData?[indexPath.row] ?? Doctor_request())
        return cell
    }

    func setupData(cell : CalendarTableViewCell,data : Doctor_request){
        cell.labelName.text = "\(data.patient?.patient_name ?? "") "
        cell.labelTreatment.text = "\(data.patient?.booking_for ?? "")"
        cell.labelTime.text = "\(data.consult_time ?? "")"
        cell.buttonOptions.isHidden = false
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = AppointmentDetailsViewController.initVC(storyBoardName: .main, vc: AppointmentDetailsViewController.self, viewConrollerID: Storyboard.Ids.AppointmentDetailsViewController)
////        vc.appoinment = self.all_appointments[indexPath.row]
//        self.push(from: self, ToViewContorller: vc)
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.3

    }
}

extension CalendarViewController : CLWeeklyCalendarViewDelegate{
    func dailyCalendarViewDidSelect(_ date: Date!) {
        print("SelectedDate" , date)
        let datestr = dateConvertor(date.description, _input: .date_time, _output: .YMD)
        self.selectedDate = datestr
        getAppointmentDetail(date: date.description)
    }
    
    
}

//Api calls
extension CalendarViewController : DoctorPresenterToDoctorViewProtocol{
    
    func getcalenderlist(data: calendermodel) {
        self.calenderdetail = data
    }
    
    func getCalenderAvailableData(data: calenderAvailableModel) {
        var calData : calenderAvailableModel = calenderAvailableModel()
        calData = data
        calData.responseData = calData.responseData?.filter({$0.status != "CANCELLED"})
        self.calendarData = calData
        self.listTable.reloadData()
    }
   
    
    func getAppointmentDetail(date : String){
       
        self.doctorPresenter?.getAppointmentDetail(date: dateConvertor(date.description, _input: .date_time_Z, _output: .DMYs))
    }

    func cancelAppointmentDetail(id : String){
        let param : Parameters = ["id" : id]
        self.doctorPresenter?.getcancelPatient(param: param)
        
        
    }
    
    func getBlockList(block: BlockListModel) {
        self.getAppointmentDetail(date: self.calendarView.selectedDate.description)
    }
}

extension CalendarViewController : AlertDelegate{
    func selectedDate(selectionType: String, date: String, alertVC: UIViewController) {
  
    }
    
    func selectedDateTime(selectionType: DateselectionOption,date : Date, datestr: String, time: String, alertVC: UIViewController) {
       
    }
    
    func selectedTime(time: String, alertVC: UIViewController) {
        if #available(iOS 13.0, *) {
            let vc = self.storyboard?.instantiateViewController(identifier: Storyboard.Ids.PatientsViewController) as! PatientsViewController
            vc.selectedDate = self.selectedDate + " " + time
            vc.isFromCalendar = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
      
    
    
}

}

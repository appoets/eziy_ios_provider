//
//  Constants.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 19/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//
import Foundation
import AudioToolbox
import UIKit


struct Constants {
    
    static let string = Constants()
    
    let english = "English"
    let lao = "Lao"
    let thai = "Thai"
    let khmer = "Cambodian"
    let myanmar = "Myanmar"
    let language = "Language"
    
    let selectLanguage = "Select Language"
    
    let Done = "Done"
    let Back = "Back"
   
    let noDevice = "no device"
    
    let edit = "Edit"
    let manual = "manual"
    let OK = "OK"
    let Cancel = "Cancel"
    let NA = "NA"
    let MobileNumber = "Mobile Number"
    let next = "Next"
    let selectSource = "Select Source"
    let camera = "Camera"
    let photoLibrary = "Photo Library"
    let logout = "Logout"
    let changeLanguage = "Change Language"
    let cannotMakeCallAtThisMoment = "Cannot make call at this moment"
    let couldnotOpenEmailAttheMoment = "Could not open Email at the moment."
    let areYouSureWantToLogout = "Are you want to logout?"
    let healthFeed = "Health Feed"
    let feedBack = "Feed Back"
    let patients = "Patients"
    let patientInformation = "Patient Information"
    
    let appointmentDetails = "Appointment Details"
    let addAppointment = "Add Appointment"
    let editAppointment = "Edit Appointment"
    let blockCalenar = "Block Calendar"
    
    let chat = "Chat"
    let editProfile = "Edit Profile"
    
    let booked = "Booked"
    let cancelled = "Cancelled"
    let newPatient = "New Patients"
    let repeatPatient = "Repeat Patients"
    let appointment = "Appointment"
    let revenu = "Revenue"
    let showDate = "Showing date"
    let change = "Change"
    let paiddata = "Paid"
    let pending = "Pending"
    let appointmentDetail = "Appointment Details"
    let Yes = "Yes"
    let No = "No"
    let wallet = "Wallet"
    let walletAmount = "Your Wallet Amount is"
    let noTransactionsYet = "No Transactions yet"
    let transactionId = "Transaction Id"
    let date = "Date"
    let amount = "Amount"
    let status = "Status"
    let transaction = "Transaction"
    let addCardPayments = "Add card for payments"
    let paymentMethods = "Payment Methods"
    let yourCards = "Your Cards"
    let login = "Login"
    let register = "Register"
    let enterPhoneLogin = "Enter phone number to login"
    let countryCode = "Country Code"
    let termsTxt = "By using the app you agree to Terms and Condition"
    let morning = "Morning"
    let evening = "Evening"
    let proceed = "Proceed"
    let daySlot = "Day Slot"
    let firstName = "First Name"
    let lastName = "Last Name"
    let emailId = "Email Id"
    let password = "Password"
    let confirmPasswd = "Confirm Password"
    let clinicName = "Clinic Name"
    let clinic = "Clinic Photo"
    let clinicEmailId = "Clinic Email Id"
    let country = "Country"
    let addresss = "Address"
    let continueTxt = "Continue"
    let registerDctr = "Register Doctor"
    let clinicDetails = "Clinic Details"
    let doctorDetails = "Doctor Details"
    let male = "Male"
    let female = "Female"
    let others = "Others"
    let mon = "Mon"
    let tue = "Tue"
    let wed = "Wed"
    let thu = "Thu"
    let fri = "Fri"
    let sat = "Sat"
    let sun = "Sun"
    let speciality = "Speciality"
    let categories = "Categories"
    let gender = "Gender"
    let consulationFees = "Consulation Fees"
    let from = "From :"
    let to = "To :"
    let specialitiesDetails = "Speciality Details"
    let timingAvailable = "Timing Available"
    let dayAvailable = "Days Available"
    let calendar = "Calender"
    let feedback = "Feedback"
    let appotiments = "Appotinments"
    let vsLast30days = "vs last 30 days"
    let selectAnOption = "Please Select an Option"
    let thisWeek = "This Week"
    let thisMonth = "This Month"
    let lastYear = "Last Year"
    let dismiss = "Dismiss"
    let addMoney = "Add Money"
    let walletBalance = "Wallet Balance"
    let availableBalance = "Available Balance"
    let enterAmount = "Enter Amount"
    let deletePatient = "Delete Patient"
    let callPatient = "Call Patient"
    let experience = "Experience"
    let positive = "Positive"
    let negative = "Negative"
    let addArticles = "Add Article"
    let titleName = "Title Name"
    let addCoverPhoto = "Add Cover Photo"
    let description = "Description"
    let city = "city"
   // let countryCode = "countryCode"
    let residency = "residency"
    let publish = "Publish"
    let addServices = "Add Services"
   let addNewCard = "+ Add New Card"
    let deleteCard = "Delete Card"
    let specialities = "Specialities"
    let healthArticle = "Health Article"
    let publishedArticles = "Published Articles"
    let doctor = "Doctor"
    let selectDateTime = "Select Date and Time"
    let writeSomething = "Write Something"
    let createAppotinmentView = "Create Appointment View"
    let patientName = "Patient Name"
    let comments = "Comments"
    let createAppoitnment = "Create Appointment"
    let addPrescription = "Add Prescription"
    let addInstructions = "Add Instruction for Prescription"
    let uploadPrescriptionImage = "Upload Prescription Image"
    let savePrescription = "Save Prescription"
    let incomingRequest = "Incoming Request"
    let accept = "Accept"
    let reject = "Reject"
    let sendOTP = "We just send 4 digit code to Your Phone Number"
    let enterOTP = "Enter OTP"
    let subscriptionList = "Subscription List"
    let subscriptionPlans = "Subscription Plans"
    let selectPatients = "Select from Exisiting Patients"
    let age = "Age"
    let scheduledDate = "Scheduled Date & Time"
    let addPatients = "Add Patients"
    let or = "or"
    let chatInfor = "You have 24 hours time to chat with the doctor, To continue after 24 hours you have to pay again."
    let bookedFor = "Booked For"
    let shareExperience = "Please share your experience"
    let rateExperience = "Rate your Experience"
    let awards = "Awards"
    let website = "Website"
    let Specilization = "Specialization"
    let location = "Location"
    let specilist = "Specialist"
    let qualification = "qualification"
    let yourProfile = "Your Profile"
    let personal = "Personal"
    let medical = "Medical"
    let Service = "Service"
    let lifestyle = "Lifestyle"
    let alcholHabit = "Do you have the habit of consuming alcohol?"
    let smokingHabit = "Do you have the habit of smoking?"
    let profileUpdated = "Profile Updated Sucessfully"
    let single = "Single"
    let married = "Married"
  
    let bloodGroup = "Blood Group"
    let dob = "Date Of Birth"
    let martialStatus = "Martial Status"
    let weight = "Weight"
    let height = "Height"
    let emergencyContact = "Emergency Contact"
    let allergies = "Allergies"
    let currentMedication = "Current Medication"
    let pastMedication = "Past Medication"
    let chronicDiseases = "Chronic Diseases"
    let injuries = "Injuries"
    let surgeries = "Suregeries"
    let activity = "Activity"
    let foodPreference = "Food Preference"
    let profile = "Profile Photo"
    let medicals = "Medical Certificate"

}

//Defaults Keys

struct Keys {
    
    static let list = Keys()
    let userData = "userData"
    
    let id = "id"
    let name = "name"
    let accessToken = "access_token"
    let latitude = "latitude"
    let lontitude = "lontitude"
    let coOrdinates = "coOrdinates"
    let firstName = "firstName"
    let lastName = "lastName"
    let picture = "picture"
    let email = "email"
    let mobile = "mobile"
    
}







// Devices

enum DeviceType : String, Codable {
    
    case ios = "ios"
    case android = "android"
    
}


//enum Language : String {
//
//    case english = "en"
//    case spanish = "es"
//
//}



enum defaultSystemSound : Float {
    case peek = 1519
    case pop = 1520
    case cancelled = 1521
    case tryAgain = 1102
    case Failed = 1107
}





//
//  CreateHealthFeedViewController.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 29/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class CreateHealthFeedViewController: UIViewController {
    @IBOutlet weak var labelAddArticleString: UILabel!
    @IBOutlet weak var textFieldTitle: HoshiTextField!
    @IBOutlet weak var labelAddCoverPhotoString: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var labelDescriptionString: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var backbtn: UIImageView!
    @IBOutlet weak var buttonPublish: UIButton!
    @IBOutlet weak var buttonAddImage: UIButton!
    @IBOutlet weak var addImg: UIImageView!
    @IBOutlet weak var covreview: UIView!
    var isImageAdded : Bool = false
    var imageData: UIImage?
    var PhotoArray = [UIImage]()
    var article : ArticleReq?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialLoads()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
}

extension CreateHealthFeedViewController {
    func initialLoads(){
        self.localize()
        self.backbtn.addTap {
            self.popOrDismiss(animation: true)
        }
        self.descriptionTextView.delegate = self
        self.coverImage.addTap {
            self.chooseCoverPhoto()
        }
        self.buttonAddImage.addTarget(self, action: #selector(chooseCoverPhoto), for: .touchUpInside)
        self.setupAction()
    }
    override func viewWillLayoutSubviews() {
       let frame = CGRect(x: 10, y: 10, width: self.view.frame.width - 20, height: 300)
       self.coverImage.frame = frame
        covreview.addShadow(offset: CGSize(width: 0, height: 0), color: .lightGray, radius: 2 , opacity: 0.4)
    }
    
    func setupAction(){
        self.buttonPublish.addTap {
            if self.validation(){
                let param:Parameters = ["title":self.textFieldTitle.getText,
                                        "description":self.descriptionTextView.text ?? "","image":self.article?.image ?? ""
                                        ]
                var profileImageData: Data!
                if  let profileData = self.coverImage.image?.jpegData(compressionQuality: 0.5) {
                    profileImageData = profileData
                }
                self.doctorPresenter?.addhealth(param: param, imagedata : ["image" : profileImageData])
                
            }
        }
    }
    
    @objc func chooseCoverPhoto(){
       
    }
    
    
    func validation() -> Bool{
        if self.textFieldTitle.getText.isEmpty{
            
            showToast(msg:ErrorMessage.list.addArticleTitle)
            return false
        }else if self.descriptionTextView.text.isEmpty{
            showToast(msg: ErrorMessage.list.addArticleDescription.localize())
            return false
        }
        else{
            return true
        }
    }
    
    private func localize(){
        self.labelAddArticleString.text = Constants.string.addArticles.localize()
        self.textFieldTitle.placeholder = Constants.string.titleName.localize()
        self.labelAddCoverPhotoString.text = Constants.string.addCoverPhoto.localize()
        self.labelDescriptionString.text = Constants.string.description.localize()
        self.buttonPublish.setTitle(Constants.string.publish.localize(), for: .normal)
    }
}


//Api calls
extension CreateHealthFeedViewController : DoctorPresenterToDoctorViewProtocol{
    func addhealthfeed(addhealthfeed: ArticleReq) {
        self.article = addhealthfeed
        ToastManager.show(title: "Created Successfully" , state: .success)
        self.navigationController?.popViewController(animated: true)
}
    }

extension CreateHealthFeedViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
}



//
//  DashBoardViewController.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 19/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper
import NVActivityIndicatorView

class DashBoardViewController: UIViewController {

    @IBOutlet weak var categoriesCollection: UICollectionView!
    
    @IBOutlet weak var userNameLbl: UILabel!
//    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var dashView: UIView!

    @IBOutlet weak var bookedLbl: UILabel!
    @IBOutlet weak var cancelledLbl: UILabel!
    @IBOutlet weak var pendinglbl: UILabel!
    @IBOutlet weak var newPatientLbl: UILabel!
    @IBOutlet weak var paidlabl: UILabel!
    @IBOutlet weak var repeatLbl: UILabel!
    @IBOutlet weak var revenueLbl: UILabel!
    @IBOutlet weak var appointmentDateLbl: UILabel!
    @IBOutlet weak var appointmentCountLbl: UILabel!
    @IBOutlet weak var showDateLbl: UILabel!
    @IBOutlet weak var chageDateBtn: UIButton!
    @IBOutlet weak var shadowLbl: UILabel!

    
    @IBOutlet weak var bookedTitleLbl: UILabel!
    @IBOutlet weak var cancelledTitleLbl: UILabel!
    @IBOutlet weak var newPatientTitleLbl: UILabel!
    @IBOutlet weak var repeatTitleLbl: UILabel!
//    @IBOutlet weak var paidBtn: UIButton!
//    @IBOutlet weak var pendingBtn: UIButton!
    @IBOutlet weak var appointmentDateTitleLbl: UILabel!
    @IBOutlet weak var showDateTitleLbl: UILabel!
    
    @IBOutlet weak var buttonProfile: UIButton!
    var activityIndicatorView: NVActivityIndicatorView!
    let titles = [Constants.string.calendar.localize(),Constants.string.patients.localize(),Constants.string.feedback.localize(),Constants.string.healthFeed.localize()]
//                  Constants.string.chat.localize()
                   //Constants.string.wallet.localize()
    let titlesImages = ["calender","patient","feedback","health"] //trendingx
//    ,"chat"
    var revenuelist : revenuemodel?
    var getHistoryData : GetAppointmentHistory?
    private lazy var loader  : UIView = {
        return createActivityIndicator(self.view)
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    var timerGetRequest: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        doctorPresenter?.getBaseURL(param: [Constant.saltkey: APPConstant.saltKeyValue])
        // Do any additional setup after loading the view.
        initialLoads()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.doctorPresenter?.getAppointmentHistory()
        self.doctorPresenter?.getrevenue()
        self.doctorPresenter?.getProfileDetail()
        
       
        self.navigationController?.isNavigationBarHidden = true
        let date = dateConvertor(Date().description, _input: .date_time, _output: .YMD)
   //     self.getDashboardDetail(fromdate: date, todate: date)
        let startDate = Date().startOfWeek
        let sDate = dateConvertor(startDate?.description ?? "", _input: .date_time_Z, _output: .YMD)
        let endWeek = Date().endOfWeek
        let eDate = dateConvertor(endWeek?.description ?? "", _input: .date_time_Z, _output: .YMD)
        self.getDashboardDetail(fromdate: sDate, todate: eDate)
       
//        self.profileApi()
         timerGetRequest = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.checkChatRequest), userInfo: nil, repeats: true)
         NotificationCenter.default.addObserver(self, selector: #selector(self.inValidateTimer(_:)), name: NSNotification.Name(rawValue: "InValidateTimer"), object: nil)
        DispatchQueue.main.async {
            self.showTabBar()
            self.enableTabBar()
        }
        
//        self.profileApi()
    }
    
    
    func getDashboardDetail(fromdate : String, todate : String){
  //      let url = "\(Base.home.rawValue)?from_date=\(fromdate)&to_date=\(todate)"
//        self.doctorPresenter?.dateFilerFormate(start: fromdate, end: todate)
//        self.presenter?.HITAPI(api: url, params: nil, methodType: .GET, modelClass: DashBoardEntity.self, token: true)
        let startDate = Date().startOfWeek
        let sDate = dateConvertor(startDate?.description ?? "", _input: .date_time_Z, _output: .YMD)
        let endWeek = Date().endOfWeek
        let eDate = dateConvertor(endWeek?.description ?? "", _input: .date_time_Z, _output: .YMD)
        showDateLbl.text = "\(dateConvertor(fromdate, _input: .YMD, _output: .DM)) - \(dateConvertor(todate, _input: .YMD, _output: .DM))"
//        self.loader.isHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        timerGetRequest?.invalidate()
        timerGetRequest = nil
    }
    @objc func inValidateTimer(_ notification: NSNotification) {
        timerGetRequest?.invalidate()
        timerGetRequest = nil
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.dashView.cornerRadius = 8.0
    }

    
    @objc private func checkChatRequest(){
//        self.getChatRequest()
    }
    

}

extension DashBoardViewController {
    
    func initialLoads(){
        
        registerCell()
        
//        self.userImg.addTap {
//
//            self.ontapProfile()
//        }
//        self.userImg.makeRoundedCorner()
        self.setupLanguage()
        self.chageDateBtn.addTarget(self, action: #selector(changeAction(_sender:)), for: .touchUpInside)
    }
    private func localize(){
        self.appointmentDateTitleLbl.text = Constants.string.appotiments.localize()
        self.appointmentDateLbl.text = Constants.string.vsLast30days.localize()
        
    }
    func registerCell(){
        
        self.categoriesCollection.register(UINib(nibName: "HomeOptionCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "HomeOptionCollectionViewCell")
    }
    
    @objc func ontapProfile(){
        
        self.push(id: Storyboard.Ids.ProfieViewController, animation: true)
    }
    func setData(){
        let data = self.getHistoryData?.responseData
        self.bookedLbl.text = data?.booked_count?.description ?? "0"
            self.cancelledLbl.text = data?.cancelled_count?.description ?? "0"
            self.newPatientLbl.text = data?.patient_count?.description ?? "0"
            self.repeatLbl.text = data?.repeated_count?.description ?? "0"
        self.userNameLbl.text =
//        data?.doctor_details?.first?.first_name ?? ""
        "\(data?.doctor_details?.first?.provider?.first_name ?? "")\(data?.doctor_details?.first?.provider?.last_name ?? "")"
       

            self.appointmentCountLbl.text = data?.total_appointment_count?.description ?? "0"

    }
    func setrevenuedata(){
        let data = self.revenuelist?.responseData

        self.paidlabl.text = "\(Constants.string.paiddata.localize()): \(APPConstant.curreny) \(data?.total_paid_revenue ?? 0)"
        self.pendinglbl.text = "\(Constants.string.pending.localize()): \(APPConstant.curreny) \(data?.pending_revenue ?? 0)"
        self.revenueLbl.text =
    "\(Constants.string.revenu.localize()): \(APPConstant.curreny) \(data?.total_paid_revenue ?? 0)"
        
    }
 
    func setupLanguage() {
        self.bookedTitleLbl.text = Constants.string.booked.localize()
        self.cancelledTitleLbl.text = Constants.string.cancelled.localize()
        self.newPatientTitleLbl.text = Constants.string.newPatient.localize()
        self.repeatTitleLbl.text = Constants.string.repeatPatient.localize()
        self.appointmentDateTitleLbl.text = Constants.string.appointment.localize()
        self.showDateTitleLbl.text = Constants.string.showDate.localize()
        self.chageDateBtn.setTitle(Constants.string.change.localize(), for: .normal)
    }
//    func checkAlreadyLogin() -> Bool {
//        let fetchData = try! DataBaseManager.shared.context.fetch(LoginData.fetchRequest()) as? [LoginData]
//        if (fetchData?.count ?? 0) <= 0 {
//            return false
//        }
//        AppManager.share.accessToken = fetchData?.first?.access_token
//        print("Access Token \(fetchData?.first?.access_token ?? .Empty)" )
//        return (fetchData?.count ?? 0) > 0
//    }
    
    @IBAction private func changeAction(_sender:UIButton){
        let alert = UIAlertController(title: Constants.string.appotiments.localize(), message: Constants.string.selectAnOption.localize(), preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: Constants.string.thisWeek.localize(), style: .default , handler:{ (UIAlertAction)in
            print("This Week")
            let startDate = Date().startOfWeek
            let sDate = dateConvertor(startDate?.description ?? "", _input: .date_time_Z, _output: .YMD)
            let endWeek = Date().endOfWeek
            let eDate = dateConvertor(endWeek?.description ?? "", _input: .date_time_Z, _output: .YMD)
            self.getDashboardDetail(fromdate: sDate, todate: eDate)
            
        }))
        
        alert.addAction(UIAlertAction(title: Constants.string.thisMonth.localize(), style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            let startDate = Date().startOfMonth
            let sDate = dateConvertor(startDate.description , _input: .date_time_Z, _output: .YMD)
            let endWeek = Date().endOfMonth
            let eDate = dateConvertor(endWeek.description , _input: .date_time_Z, _output: .YMD)
            self.getDashboardDetail(fromdate: sDate, todate: eDate)
            
        }))

        alert.addAction(UIAlertAction(title: Constants.string.lastYear.localize(), style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
            let startDate = Date().previousYear(Date())
            let sDate = dateConvertor(startDate.description , _input: .date_time, _output: .YMD)
            let endWeek = Date().endOfMonth
            let eDate = dateConvertor(endWeek.description , _input: .date_time, _output: .YMD)
            self.getDashboardDetail(fromdate: sDate, todate: eDate)
        }))
        
        alert.addAction(UIAlertAction(title: Constants.string.dismiss.localize(), style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        
        //uncomment for iPad Support
        //alert.popoverPresentationController?.sourceView = self.view

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    
}
//MARK:- COLLECTION VIEW DELEGATES AND DATASOURCES :

extension DashBoardViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoriesCollection.dequeueReusableCell(withReuseIdentifier: "HomeOptionCollectionViewCell", for: indexPath) as! HomeOptionCollectionViewCell
        cell.labelTitle.text = titles[indexPath.item]
        cell.typeImage.image = UIImage(named: titlesImages[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if indexPath.item == 3 {

            self.push(id: Storyboard.Ids.HealthFeedViewController, animation: true)
        }

        if indexPath.item == 2 {
            self.push(id: Storyboard.Ids.FeedBackViewController, animation: true)
        }
//
        if indexPath.item == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.PatientsViewController) as! PatientsViewController
          //  vc.isFromCalendar = false
            self.navigationController?.pushViewController(vc, animated: true)

        }

//        if indexPath.item == 3 {
//
//            self.push(id: Storyboard.Ids.ChatViewController, animation: true)
//        }
//
        if indexPath.item == 0 {
            self.push(id: Storyboard.Ids.CalendarViewController, animation: true)
        }
////
//        if indexPath.item == 0 {
//            self.push(id: Storyboard.Ids.WalletViewController, animation: true)
//        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.categoriesCollection.frame.width/2.1, height: 80)
    }

}
//Api calls
extension DashBoardViewController : DoctorPresenterToDoctorViewProtocol{
    func viewProfileDetail(profileEntity: ProfileEntity) {
        if let userDetails = profileEntity.responseData {
            AppManager.share.setUserDetails(details: userDetails)
            
        }
       
       
    }
    
    func getAppointmentHistory(data: GetAppointmentHistory) {
        self.getHistoryData = data
        self.setData()
    }
    func getrevenue(data: revenuemodel) {
        self.revenuelist = data
        self.setrevenuedata()
}
}

extension DashBoardViewController : IncomingRequestDelegate{
    func acceptButtonAction(_ sender: UIButton) {
        self.push(id: Storyboard.Ids.ChatViewController, animation: true)
    }
    
    func rejectButtonAction(_ sender: UIButton) {
        
    }
    
    func finishButtonAction() {
        
    }
    
    
}



extension Date {
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 0, to: sunday)
    }

    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 6, to: sunday)
    }
    
    var startOfMonth: Date {

        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: self)

        return  calendar.date(from: components)!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonth)!
    }
    
    func previousYear(_ date: Date) -> Date {
        let calendar = NSCalendar.current
        return calendar.date(byAdding: .year, value: -1, to: date)!
    }
}



extension UIView{
    
    func addingCornerandShadow(color : UIColor = .gray, opacity : Float = 0.5, offset : CGSize = CGSize(width: 0.5, height: 0.5), radius : CGFloat = 0.5,corner:CGFloat = 0) {
            var shadowLayer: CAShapeLayer!

            if shadowLayer == nil {
                shadowLayer = CAShapeLayer()
                shadowLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: corner).cgPath
                shadowLayer.fillColor = UIColor.white.cgColor

                shadowLayer.shadowColor = color.cgColor
                shadowLayer.shadowPath = shadowLayer.path
                shadowLayer.shadowOffset = offset
                shadowLayer.shadowOpacity = opacity
                shadowLayer.shadowRadius = radius

                self.layer.insertSublayer(shadowLayer, at: 0)
                //layer.insertSublayer(shadowLayer, below: nil) // also works
            }
        }

}


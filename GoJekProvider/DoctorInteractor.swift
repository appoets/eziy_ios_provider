//
//  DoctorInteractor.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 19/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import Alamofire
//MARK:- doctorPresentorTodoctorInterectorProtocol

class DoctorInteractor: DoctorPresentorToDoctorInterectorProtocol {
    
    func getcancelPatient(param: Parameters) {
        let url = URLConstant.cancelPatient
        WebServices.shared.requestToApi(type: BlockListModel.self, with:  url, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getBlockList(block: response)
            }}
    }
    
    
    func blockCalendar(param : Parameters) {
        let url = URLConstant.blockCalendar
        WebServices.shared.requestToApi(type: BlockListModel.self, with:  url, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getBlockList(block: response)
            }}
    }
    
    
    func getAppointmentDetail(date : String) {
        let url = URLConstant.appointmentDetail+"?schedule_date=\(date)"
        WebServices.shared.requestToApi(type: calenderAvailableModel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getCalenderAvailableData(data: response)
            }}
    }
    
    func dateFilerFormate(start: String, end: String) {
        var url = "\(URLConstant.doctorHospitalProfile)?from_date=\(start)&to_date=\(end)"
        WebServices.shared.requestToApi(type: ProfileEntity.self, with: url, urlMethod: .get, showLoader: true, params: nil, encode: URLEncoding.default, completion: { [weak self] response in
            guard let self = self else {
                return
            }
            if let responseValue = response?.value {
                self.doctorPresenter?.viewProfileDetail(profileEntity: responseValue)
            }
        })
    }
    
    func getProfileDetail() {
        WebServices.shared.requestToApi(type: ProfileEntity.self, with: URLConstant.KProfiledetail, urlMethod: .get, showLoader: true, params: nil, encode: URLEncoding.default, completion: { [weak self] response in
            guard let self = self else {
                return
            }
            if let responseValue = response?.value {
                self.doctorPresenter?.viewProfileDetail(profileEntity: responseValue)
            }
        })
    }
    
    func deletePatient(id: String) {
        let url = URLConstant.deletePatient + "/\(id)"
        WebServices.shared.requestToApi(type: DeletePatient.self, with:  url, urlMethod: .delete, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.deletePatient(data: response)
            }}
    }
    
    func getrevenue() {
        let url = URLConstant.krevenewdata
        WebServices.shared.requestToApi(type: revenuemodel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getrevenue(data: response)
            }}
    }
    
    func getAppointmentHistory() {
        let url = URLConstant.appointmentHistory
        WebServices.shared.requestToApi(type: GetAppointmentHistory.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getAppointmentHistory(data: response)
            }}
    }
    
    func gettimelist() {
        let url = URLConstant.ktimeslot
        WebServices.shared.requestToApi(type: TimeSlotmodel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.gettimelist(data: response)
            }}
    }
    
    
    func getfeedbacklist() {
        let url = URLConstant.kfeedback
        WebServices.shared.requestToApi(type: Feedback.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getfeedbacklist(data: response)
            }}
    }
    
    func getcalenderlist() {
        let url = URLConstant.kcalenderlist
        WebServices.shared.requestToApi(type: calendermodel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getcalenderlist(data: response)
            }}
    }
    
    func getPatientlist() {
        let url = URLConstant.kcalenderlist
        WebServices.shared.requestToApi(type: PatientList.self, with: url, urlMethod: .get, showLoader: true, params: nil, accessTokenAdd: true, failureReturen: true, completion: { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getPatientlist(data: response)
            }})
    }
    
    func addhealth(param: Parameters,imagedata : [String : Data]) {
        WebServices.shared.requestToApi(type: ArticleReq.self, with: URLConstant.kaddArticle, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.addhealthfeed(addhealthfeed: response)
            }
        }
    }
    
    func gethealthfeed() {
        let url = URLConstant.khealthfeed
        WebServices.shared.requestToApi(type: HealthFeedmodel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.gethealthfeed(data: response)
            }}
    }
    
    func getservicelist() {
        let url = URLConstant.kservicelist
        WebServices.shared.requestToApi(type: servicemodel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getservicelist(data: response)
            }}
    }
    func getslotlist() {
        let url = URLConstant.kslotlist
        WebServices.shared.requestToApi(type: Slotmodel.self, with:  url, urlMethod: .get, showLoader: true,params: nil) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.getslotlist(data: response)
            }}
        
    }
    func profiledata(param: Parameters, imagedata: [String : Data]) {
        WebServices.shared.requestToApi(type: profileaboutentity.self, with: URLConstant.kprofileAndupdate, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
            guard let self = self else {
                return
            }
            if let response = response?.value {
                self.doctorPresenter?.profiledata(data: response)
            }
        }
    }
  
//    func profiledata(param: Parameters, imagedata: [String : Data]) {
////        WebServices.shared.requestToApi(type: .self, with: URLConstant.kprofile, urlMethod: .post, showLoader: true,params: param) { [weak self] (response) in
////            guard let self = self else {
////                return
////            }
////            if let response = response?.value {
////                self.doctorPresenter?.profiledata(slotmodel: response)
////            }
////        }
//    }

    var doctorPresenter: DoctorInterectorToDoctorPresenterProtocol?
    
   
}

//
//  DoctorPresenter.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 19/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import Alamofire

//MARK:- doctorViewToXuberPresenterProtocol

class DoctorPresenter: DoctorViewToDoctorPresenterProtocol {
    func getcancelPatient(param: Parameters) {
        doctorInterector?.getcancelPatient(param: param)
    }
    
    
    func getAppointmentDetail(date : String) {
        doctorInterector?.getAppointmentDetail(date : date)
    }
    
    func blockCalendar(param : Parameters) {
        doctorInterector?.blockCalendar(param : param)
    }
    
    func dateFilerFormate(start: String, end: String) {
        doctorInterector?.dateFilerFormate(start: start, end: end)
    }
    
    func deletePatient(id: String) {
        doctorInterector?.deletePatient(id: id)
    }
    
    //Get profile detail
    func getProfileDetail() {
        doctorInterector?.getProfileDetail()
    }
    
    func getrevenue() {
        doctorInterector?.getrevenue()
    }
    
    func getPatientlist() {
        doctorInterector?.getPatientlist()
    }
    
    
    func postDeleteList() {
        
    }
    
    func getAppointmentHistory() {
        doctorInterector?.getAppointmentHistory()
    }
    
    func gettimelist() {
        doctorInterector?.gettimelist()
    }
    
    func profiledata(param: Parameters, imagedata: [String : Data]) {
        doctorInterector?.profiledata(param: param, imagedata: imagedata)
    }
    
    func getfeedbacklist() {
        doctorInterector?.getfeedbacklist()
    }
    
    func getcalenderlist() {
        doctorInterector?.getcalenderlist()
    }
    
    func addhealth(param: Parameters,imagedata : [String : Data]) {
        doctorInterector?.addhealth(param: param, imagedata: imagedata)
    }
    
    
    func gethealthfeed() {
        doctorInterector?.gethealthfeed()
    }
    func getservicelist() {
        doctorInterector?.getservicelist()
    }
    func getslotlist() {
        doctorInterector?.getslotlist()
    }
    
   
    
    
    
    var doctorView: DoctorPresenterToDoctorViewProtocol?;
    var doctorInterector: DoctorPresentorToDoctorInterectorProtocol?;
    var doctorRouter: DoctorPresenterToDoctorRouterProtocol?
    
    
    
    
    
}

//MARK:- doctorInterectorTodoctorPresenterProtocol

extension DoctorPresenter: DoctorInterectorToDoctorPresenterProtocol {
    
    
    func getBlockList(block: BlockListModel) {
        doctorView?.getBlockList(block: block)
    }
    
    func viewProfileDetail(profileEntity: ProfileEntity) {
        doctorView?.viewProfileDetail(profileEntity: profileEntity)
    }
    
    func deletePatient(data: DeletePatient) {
        doctorView?.deletePatient(data: data)
    }
    

    func getPatientlist(data: PatientList) {
        doctorView?.getPatientlist(data: data)
    }

    func getrevenue(data: revenuemodel) {
        doctorView?.getrevenue(data: data)
    }
    
    func getCalenderAvailableData(data: calenderAvailableModel) {
        doctorView?.getCalenderAvailableData(data: data)
    }
    
    func getAppointmentHistory(data: GetAppointmentHistory) {
        doctorView?.getAppointmentHistory(data: data)
    }
    
//    func profiledata(slotmodel: profilemodel) {
//        doctorView?.profiledata(slotmodel: slotmodel)
//    }
    func profiledata(data: profileaboutentity) {
        doctorView?.profiledata(data: data)
    }
    
    func getfeedbacklist(data: Feedback) {
        doctorView?.getfeedbacklist(data: data)
    }
    
    func getcalenderlist(data: calendermodel) {
        doctorView?.getcalenderlist(data: data)
    }
    
    func addhealthfeed(addhealthfeed: ArticleReq) {
        doctorView?.addhealthfeed(addhealthfeed: addhealthfeed)
    }
    
    
    func gethealthfeed(data: HealthFeedmodel) {
        doctorView?.gethealthfeed(data: data)
    }
    func getservicelist(data: servicemodel) {
        doctorView?.getservicelist(data: data)
    }
    func getslotlist(data: Slotmodel) {
        doctorView?.getslotlist(data: data)
    }
    func gettimelist(data: TimeSlotmodel) {
        doctorView?.gettimelist(data: data)
    }
    
    
    
}



//
//  DoctorProtocol.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 19/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import Alamofire

var doctorPresenterObject: DoctorViewToDoctorPresenterProtocol?

//MARK:- doctor presenter to doctor viewcontroller
//Backward process
protocol DoctorPresenterToDoctorViewProtocol: class {
    func gethealthfeed(data : HealthFeedmodel)
    func addhealthfeed(addhealthfeed : ArticleReq)
    func getcalenderlist(data : calendermodel)
    func getfeedbacklist(data : Feedback)
    func getservicelist(data : servicemodel)
    func getslotlist(data : Slotmodel)
    func profiledata(data: profileaboutentity)
    func gettimelist(data : TimeSlotmodel)
    func getAppointmentHistory(data : GetAppointmentHistory)

    func getPatientlist(data : PatientList)

   
    func getrevenue(data : revenuemodel)
    func deletePatient(data : DeletePatient)
    func viewProfileDetail(profileEntity: ProfileEntity)
    
    func getCalenderAvailableData(data : calenderAvailableModel)
    
    func getBlockList(block : BlockListModel)



}

extension DoctorPresenterToDoctorViewProtocol {
    var doctorPresenter: DoctorViewToDoctorPresenterProtocol? {
        get {
            doctorPresenterObject?.doctorView = self
            return doctorPresenterObject
        }
        set(newValue) {
            doctorPresenterObject = newValue
        }
    }
    func gethealthfeed(data : HealthFeedmodel){return}
    func addhealthfeed(addhealthfeed : ArticleReq) { return }
    func getcalenderlist(data : calendermodel){ return }
    func getfeedbacklist(data : Feedback){ return }
    func getservicelist(data : servicemodel){ return}
    func getslotlist(data : Slotmodel){return}
    func profiledata(data: profileaboutentity){return}
    func gettimelist(data : TimeSlotmodel){return}
    func getAppointmentHistory(data : GetAppointmentHistory){return}

    func getPatientlist(data : PatientList){return}

  
    func getrevenue(data : revenuemodel){return}
    func deletePatient(data : DeletePatient){return}
    func viewProfileDetail(profileEntity: ProfileEntity){return}
    
    func getCalenderAvailableData(data : calenderAvailableModel){return}
    
    func getBlockList(block : BlockListModel){return}

}

//MARK:- doctor interector to doctor presenter
//Backward process
protocol DoctorInterectorToDoctorPresenterProtocol {
    //Cancel request response
    func gethealthfeed(data : HealthFeedmodel)
    func addhealthfeed(addhealthfeed : ArticleReq)
    func getcalenderlist(data : calendermodel)
    func getfeedbacklist(data : Feedback)
    func getservicelist(data : servicemodel)
    func getslotlist(data : Slotmodel)
    func profiledata(data: profileaboutentity)
    func gettimelist(data : TimeSlotmodel)
    func getAppointmentHistory(data : GetAppointmentHistory)
    func getPatientlist(data : PatientList)
    func getrevenue(data : revenuemodel)
    func deletePatient(data : DeletePatient)
    func viewProfileDetail(profileEntity: ProfileEntity)
    func getCalenderAvailableData(data : calenderAvailableModel)
    func getBlockList(block : BlockListModel)

}

//MARK:- doctor presenter to doctor interector
//Forward process
protocol DoctorPresentorToDoctorInterectorProtocol {
    
    var doctorPresenter: DoctorInterectorToDoctorPresenterProtocol? {get set}
    
   
    func gethealthfeed()
    func addhealth(param: Parameters,imagedata : [String : Data])
    func getcalenderlist()
    func getfeedbacklist()
    func getservicelist()
    func getslotlist()
    func gettimelist()
    func getAppointmentHistory()
    func getAppointmentDetail(date : String)
    func blockCalendar(param : Parameters)
    func getPatientlist()
    func getcancelPatient(param : Parameters)
    func dateFilerFormate(start : String,end : String)
    func profiledata(param: Parameters,imagedata : [String : Data])
    func getrevenue()
    func deletePatient(id:String)
    func getProfileDetail()
 
//    func profiledata(param: Parameters,imagedata : [String : Data])

    
    
   
   
}

//MARK:- doctor view to doctor presenter
//Forward process
protocol DoctorViewToDoctorPresenterProtocol {
    
    var doctorView: DoctorPresenterToDoctorViewProtocol? {get set}
    var doctorInterector: DoctorPresentorToDoctorInterectorProtocol? {get set}
    var doctorRouter: DoctorPresenterToDoctorRouterProtocol? {get set}
    
    func gethealthfeed()
    func addhealth(param: Parameters,imagedata : [String : Data])
    func getcalenderlist()
    func getfeedbacklist()
    func getservicelist()
    func getslotlist()
    func gettimelist()
    func getAppointmentDetail(date : String)
    func getcancelPatient(param : Parameters)
    func getAppointmentHistory()
    func getPatientlist()
    func dateFilerFormate(start : String,end : String)
    func blockCalendar(param : Parameters)
    func profiledata(param: Parameters,imagedata : [String : Data])
    func getrevenue()
    func deletePatient(id:String)
    func getProfileDetail()
   
//    func profiledata(param: Parameters,imagedata : [String : Data])

    
}

//MARK:- doctor presenter to doctor router
//Forward process
protocol DoctorPresenterToDoctorRouterProtocol {
    
    static func createdoctorModule() -> UIViewController
    
}


class DepentA{
    init(){
        
//        var dd = DepentB(da: self)
        let df = DepentB()
        df.dA = self
//        df.paramerize(da: self)
    }
}
class DepentB{
    var dA : DepentA?
    init(){}
    init(da : DepentA){
        self.dA = da
    }
    
    func paramerize(da : DepentA){
        self.dA = da
    }
}

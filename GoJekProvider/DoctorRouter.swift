//
//  DoctorRouter.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 19/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class DoctorRouter: DoctorPresenterToDoctorRouterProtocol {
    static func createdoctorModule() -> UIViewController {
        let doctorDetailViewController  = DoctorStoryboard.instantiateViewController(withIdentifier: Storyboard.Ids.DashBoardViewController) as! DashBoardViewController
        var doctorPresenter: DoctorViewToDoctorPresenterProtocol & DoctorInterectorToDoctorPresenterProtocol = DoctorPresenter()
        var doctorInteractor: DoctorPresentorToDoctorInterectorProtocol = DoctorInteractor()
        let doctorRouter: DoctorPresenterToDoctorRouterProtocol = DoctorRouter()
        doctorDetailViewController.doctorPresenter = doctorPresenter
        doctorPresenter.doctorView = doctorDetailViewController
        doctorPresenter.doctorRouter = doctorRouter
        doctorPresenter.doctorInterector = doctorInteractor
        doctorInteractor.doctorPresenter = doctorPresenter
        return doctorDetailViewController
    }
    
    
    
        
//        let xuberHomeViewController  = XuberStoryboard.instantiateViewController(withIdentifier: XuberConstant.xuberHomeViewController) as! XuberHomeViewController
//        var xuberPresenter: XuberViewToXuberPresenterProtocol & XuberInterectorToXuberPresenterProtocol = XuberPresenter()
//        var xuberInteractor: XuberPresentorToXuberInterectorProtocol = XuberInteractor()
//        let xuberRouter: XuberPresenterToXuberRouterProtocol = XuberRouter()
//
//        xuberHomeViewController.xuberPresenter = xuberPresenter
//        xuberPresenter.xuberView = xuberHomeViewController
//        xuberPresenter.xuberRouter = xuberRouter
//        xuberPresenter.xuberInterector = xuberInteractor
//        xuberInteractor.xuberPresenter = xuberPresenter
//        return xuberHomeViewController
 
    
    static var DoctorStoryboard: UIStoryboard {
        return UIStoryboard(name:"Doctor",bundle: Bundle.main)
    }
}

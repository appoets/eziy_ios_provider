//
//  ErrorMessage.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 29/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation

struct ErrorMessage {
    
    static let list = ErrorMessage()
    
    let serverError = "Server Could not be reached. \n Try Again"
    let notReachable = "The Internet connection appears to be offline."
    let enterEmail = "Enter Email Id"
    let enterValidEmail = "Enter Valid Email Id"
    let enterPassword = "Enter Password"
    let enterConfirmPassword = "Enter Confirm Password"
    let enterName = "Enter Name"
    let enterPhoneNumber = "Enter Phone Number"
    let vehiclePhotoIsEmpty = "Attach vehicle Photo"
    let enterDriverLicence = "Attach Drriver License"
    let enterVehicleMake = "Enter vehicle Make"
    let enterVehicleModel = "Enter vehicle Model"
    let enterVehicleColor = "Enter vehicle Color"
    let enterPlateNumber = "Enter License plate Number"
    let enterCity = "Enter city"
    let enterCarCategory = "Enter Car Category"
    let selectTriptype = "Select Trip Type"
    let selectOutStationType  = "Select Out Station Type"
    let agreeTheTerms = "Agree The Terms and Condition"
    let enterOTP = "Enter OTP"
    let enterNewPwd = "Enter New Password"
    let enterOldPassword = "Enter Old Password"
    let enterConfirmPwd = "Enter Confirm Password"
    let EnterCountry = "Enter Country code"
    let enterAreaCode = "Enter Area Code"
    let enterTimeZone = "Enter Time Zone"
    let enterReferralCode = "Enter referral Code"
    let enterFirstName = "Enter First Name to Continue"
    let medicalschool = "Enter medical School to Continue"
    let medicalassschool = "Enter medicalAssocName to Continue"
    let certification = "Enter certification to Continue"
    let internship = "Enter internship to Continue"
    let experience = "Enter experience to Continue"
    let affiliations = "Enter affiliations to Continue"
    let enterLastName = "Enter Last Name to Continue"
    let enterPersonalEmail = "Enter Personal Email to Continue"
    let awards = "Enter Your Awards"
    let enterResidency = "Enter Residency"
    let enterClinicName = "Enter Clinic Name to Continue"
    let enterClinicEmail = "Enter Clinic Email to Continue"
    let enterMobileNumber = "Enter Mobile Number to Continue"
    let enterprofile = "clinic image field is required"
    let enterAddress = "Enter Address to Continue"
    let entercountryname = "Enter Countryname to Continue"
    let enterpostelcode = "Enter Postalcode to Continue"
    let entercity = "Enter City to Continue"
    let enterGender = "Enter Gender to continue"
    let enterSpeiciality = "Enter Speciality to continue"
    let enterFromTime =  "Enter Availablity From Time to Continue"
    let enterToTime = "Enter Availablity Till Time to Continue"
    let enterServide = "Enter Services to continue"
    let enterConsultaion = "Enter Consulation Fees"
    let enterAmount = "Enter Amount to Proceed"
    let addArticleImage = "Add Image for your Article"
    let addArticleTitle = "Enter your Article Title"
    let addArticleDescription = "Please Describe about your Article"
    let description = "please enter description"
    let website = "please enter website"
    let enterDOB = "Enter DOB"
    let enterBloodGroup = "Enter Blood Group"
    let enterMartialStatus = "Enter Martial Status"
    let enterHeight = "Enter Height"
    let enterWeight = "Enter Weight"
    let enterEmergencyContact = "Enter Emergency Contact"
    let enterLocation = "Enter Location"
    let enterAllergies = "Enter Allergies"
    let enterAwards = "Enter Awards"
    let enterWeb = "Enter Website"
    let enterDes = "Enter Your Description"
    let enterClientname = "Enter Client Name"
    let enterClientemail = "Enter Client EmailId"
    let enterClientAddress = "Enter Client Address"
    let enterClientcountry = "Enter Client Country"
    let enterClientCity = "Enter Client City"
    let enterClientResidency = "Enter Client Residency"
    let enterClientPostalcode = "Enter Client Postalcode"
    let entermediaclschool = "Enter Medical School"
    let entermedicalassoc = "Enter Medical assoc name"
    let entermediaclcertificate = "Enter Medical Certificate"
    let entermediaclintern = "Enter Internship"
    let entermediaclexperience = "Enter Experience"
    let entermediaclaffiliations = "Enter Affiliations"
    let enterservice = "Select Services"
    let entergender = "Select Gender"
    let enterfee = "Enter Consultation Fee"
    let entercat = "Select Category"
    let enterfrom = "Enter Avilability Time"
    let qualification = "qualification field required"
    let gender = "Gender field required"
    let speciality = "Speciality field required"
    let dataOfBirth = "dataOfBirth field required"
    let profilePhoto = "profile Photo field required"
    let medicalPhoto = "medical Photo field required"
    let DaySlot = "Day slot field required"
    let Time = "Time field required"
    let service = "service field required"
    let specialization = "specialization field required"
    let location = "Location field required"
    let clinicPhoto = "clinicPhoto field required"
   
    
    
}


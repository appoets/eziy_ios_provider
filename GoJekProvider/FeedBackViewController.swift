//
//  FeedBackViewController.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 30/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

class FeedBackViewController: UIViewController {

    @IBOutlet weak var feebacksList: UITableView!
    @IBOutlet weak var labelExperienceCount: UILabel!
    @IBOutlet weak var labelPositiveCount: UILabel!
    @IBOutlet weak var labelNegativeCount: UILabel!
    @IBOutlet weak var experienceTitleLabel: UILabel!
    @IBOutlet weak var postiveTitleLabel: UILabel!
    @IBOutlet weak var negativeTitleLabel: UILabel!
    
    var feedbacklist : Feedback?
    private lazy var loader  : UIView = {
        return createActivityIndicator(self.view)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialLoads()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.doctorPresenter?.getfeedbacklist()
    }


}

extension FeedBackViewController {
    
    func initialLoads(){
        self.localize()
        registerCells()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back").resizeImage(newWidth: 20), style: .plain, target: self, action: #selector(self.backButtonClick))

        self.navigationItem.title = Constants.string.feedBack.localize()

    }
    
    private func localize(){
        self.experienceTitleLabel.text = Constants.string.experience.localize()
        self.postiveTitleLabel.text = Constants.string.positive.localize()
        self.negativeTitleLabel.text = Constants.string.negative.localize()
    }
    
    func registerCells() {
        self.feebacksList.register(UINib(nibName: "FeedBackTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedBackTableViewCell")
    }
    
    func setDesign(){
        self.feebacksList.tableFooterView = UIView()
    }

}

extension FeedBackViewController : UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedbacklist?.responseData?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedBackTableViewCell") as! FeedBackTableViewCell
  //  self.setupCellData(cell: cell, data: self.feedbackList.feedback?[indexPath.row] ?? Feedback())
        return cell
    }
    //#4C65ED

//    2891FB - reverse
    // #A4C9FF
    func setupCellData(cell : FeedBackTableViewCell , data : Doctor_request){
        cell.labelName.text = data.patient?.patient_name ?? ""
        
//        self.setLbl(label: cell.labelVisitedFor, visited: "Visited for ", visitedFor: "\(data.visited_for ?? "")")
//        cell.labelComments.text = data.comments ?? ""
        cell.labelTime.text = dateConvertor(data.created_at ?? "", _input: .date_time, _output: .DMY)
//        if (data.experiences ?? "") == "LIKE"{
//            cell.satusImage.setImage("thumb-like")
////            cell.satusImage.setImage("Like", .yes, .green)
//        }else{
//            cell.satusImage.setImage("thumb-unlike")
////            cell.satusImage.setImage("dislike", .yes, .red)
//        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }

    func setLbl(label : UILabel , visited : String , visitedFor : String){
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: FontCustom.regular.rawValue, size: 12), NSAttributedString.Key.foregroundColor : UIColor(named: "TextForegroundColor")] //TextBlackColor

        let attrs2 = [NSAttributedString.Key.font : UIFont(name: FontCustom.regular.rawValue, size: 12), NSAttributedString.Key.foregroundColor : UIColor(named: "TextBlackColor")]

        let attributedString1 = NSMutableAttributedString(string: "\(visited)", attributes:attrs1 as [NSAttributedString.Key : Any])

        let attributedString2 = NSMutableAttributedString(string:"\(visitedFor)", attributes:attrs2 as [NSAttributedString.Key : Any])

        attributedString1.append(attributedString2)
        label.attributedText = attributedString1
    }

}


//Api calls
extension FeedBackViewController : DoctorPresenterToDoctorViewProtocol{
    func getfeedbacklist(data: Feedback) {
        self.feedbacklist = data
        self.populateData(data: data)
    }
    
    
    
    func populateData(data : Feedback){
        self.labelExperienceCount.text = data.responseData?.experience ?? "0"
        self.labelNegativeCount.text = "\(data.responseData?.negative_feedback ?? 0)"
        self.labelPositiveCount.text = "\(data.responseData?.positive_feedback ?? 0)"
        self.feebacksList.reloadData()
    }
    
    
    
}

      

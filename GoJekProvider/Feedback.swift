//
//  Feedback.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 30/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//
import Foundation
import ObjectMapper


struct Feedback : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : FeedResponseData?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct FeedResponseData : Mappable {
    var data : [Datas]?
    var positive_feedback : Int?
    var negative_feedback : Int?
    var experience : String?
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        data <- map["data"]
        positive_feedback <- map["positive_feedback"]
        negative_feedback <- map["negative_feedback"]
        experience <- map["experience"]
    }

}
struct Datas : Mappable {
    var id : Int?
    var doctor_request_id : Int?
    var doctor_profile_id : Int?
    var provider_id : Int?
    var user_id : Int?
    var company_id : Int?
    var description : String?
    var like : String?
    var created_at : String?
    var days : String?
    var patient_consult_for : String?
    var doctor_profile : String?
    var payment : Payment?
    var doctor_request : Doctor_request?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        doctor_request_id <- map["doctor_request_id"]
        doctor_profile_id <- map["doctor_profile_id"]
        provider_id <- map["provider_id"]
        user_id <- map["user_id"]
        company_id <- map["company_id"]
        description <- map["description"]
        like <- map["like"]
        created_at <- map["created_at"]
        days <- map["days"]
        patient_consult_for <- map["patient_consult_for"]
        doctor_profile <- map["doctor_profile"]
        doctor_request <- map["doctor_request"]
        payment <- map["payment"]
    }

}
struct Doctor_request : Mappable {
    var id : Int?
    var booking_id : String?
    var admin_service : String?
    var user_id : Int?
    var patient_id : Int?
    var provider_id : Int?
    var service_id : Int?
    var city_id : Int?
    var country_id : Int?
    var promocode_id : String?
    var admin_id : String?
    var time_slot_id : Int?
    var scheduled_at : String?
    var scheduled_end : String?
    var engaged_at : String?
    var checkin_at : String?
    var user_checked_in : String?
    var checkedout_at : String?
    var user_checked_out : String?
    var consult_time : String?
    var doctor_profile_timings_id : Int?
    var currency : String?
    var use_wallet : Int?
    var status : String?
    var cancelled_by : String?
    var cancel_reason : String?
    var payment_mode : String?
    var paid : Int?
    var timezone : String?
    var created_type : String?
    var created_at : String?
    var patient : Patient?
    var provider : Provider?
    init(){}
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        booking_id <- map["booking_id"]
        admin_service <- map["admin_service"]
        user_id <- map["user_id"]
        patient_id <- map["patient_id"]
        provider_id <- map["provider_id"]
        service_id <- map["service_id"]
        city_id <- map["city_id"]
        country_id <- map["country_id"]
        promocode_id <- map["promocode_id"]
        admin_id <- map["admin_id"]
        time_slot_id <- map["time_slot_id"]
        scheduled_at <- map["scheduled_at"]
        scheduled_end <- map["scheduled_end"]
        engaged_at <- map["engaged_at"]
        checkin_at <- map["checkin_at"]
        user_checked_in <- map["user_checked_in"]
        checkedout_at <- map["checkedout_at"]
        user_checked_out <- map["user_checked_out"]
        consult_time <- map["consult_time"]
        doctor_profile_timings_id <- map["doctor_profile_timings_id"]
        currency <- map["currency"]
        use_wallet <- map["use_wallet"]
        status <- map["status"]
        cancelled_by <- map["cancelled_by"]
        cancel_reason <- map["cancel_reason"]
        payment_mode <- map["payment_mode"]
        paid <- map["paid"]
        timezone <- map["timezone"]
        created_type <- map["created_type"]
        created_at <- map["created_at"]
        patient <- map["patient"]
        provider <- map["provider"]
        
    }

}
struct Patient : Mappable {
    var id : Int?
    var company_id : Int?
    var unique_id : String?
    var patient_name : String?
    var patient_email : String?
    var patient_contact_number : String?
    var payment_mode : String?
    var country_code : String?
    var status : Int?
    var appointment_type : String?
    var booking_for : String?
    var consult_for : String?
    var created_at : String?
    var single_count : String?
    var repeated_count : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        company_id <- map["company_id"]
        unique_id <- map["unique_id"]
        patient_name <- map["patient_name"]
        patient_email <- map["patient_email"]
        patient_contact_number <- map["patient_contact_number"]
        payment_mode <- map["payment_mode"]
        country_code <- map["country_code"]
        status <- map["status"]
        appointment_type <- map["appointment_type"]
        booking_for <- map["booking_for"]
        consult_for <- map["consult_for"]
        created_at <- map["created_at"]
        single_count <- map["single_count"]
        repeated_count <- map["repeated_count"]
    }

}

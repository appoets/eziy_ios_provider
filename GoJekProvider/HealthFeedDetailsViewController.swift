//
//  HealthFeedDetailsViewController.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 30/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class HealthFeedDetailsViewController: UIViewController {

    
    @IBOutlet weak var articleImage : UIImageView!
    @IBOutlet weak var articleTitle : UILabel!
    @IBOutlet weak var articleContent : UITextView!
    @IBOutlet weak var articledate : UILabel!
    @IBOutlet weak var healthArticle: UILabel!
    
    @IBOutlet weak var backbtn: UIImageView!
    var healthfeed : [healthResponseData] = [healthResponseData]()
    var titleText : String = ""
    var descriptionText : String = ""
    var timeText : String = ""
    var imagetitle : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.articleTitle.text = titleText
        self.articleImage.setURLImage(imagetitle)
        self.articleContent.text = descriptionText
        self.articledate.text = timeText
    }
    

}

extension HealthFeedDetailsViewController {
    
    func initialLoads(){
        self.setFont()
        self.articleContent.delegate = self
        self.healthArticle.text = Constants.string.healthArticle.localize()
        self.backbtn.addTap {
            self.popOrDismiss(animation: true)
            
        }
    }
    
    func setFont(){
        Common.setFont(to: self.articledate)
        Common.setFont(to: self.articleTitle,isTitle: true,size: 18)
        Common.setFont(to: self.articledate)
    }

}


extension HealthFeedDetailsViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
}


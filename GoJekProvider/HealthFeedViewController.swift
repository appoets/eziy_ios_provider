//
//  HealthFeedViewController.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 26/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

@available(iOS 13.0, *)
class HealthFeedViewController: UIViewController {

    @IBOutlet weak var tableViewHealthFeed: UITableView!
    @IBOutlet weak var backbutton: UIImageView!
    @IBOutlet weak var cretbtn: UIButton!
  
    var healthfeed : [healthResponseData] = [healthResponseData](){
        didSet{
            tableViewHealthFeed.reloadInMainThread()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        cretbtn.titleLabel?.isHidden = true
        self.doctorPresenter?.gethealthfeed()
        
    }
    
    @IBAction func createhealthbtn(_ sender: Any) {
        addHealthFeed()
    }
}

@available(iOS 13.0, *)
extension HealthFeedViewController {
    func initialLoads() {
        registerCells()
        setDesign()
        self.backbutton.addTap {
            self.popOrDismiss(animation: true)
        }

    }
    
    func registerCells() {
        self.tableViewHealthFeed.register(UINib(nibName: "HealthFeedTableViewCell", bundle: nil), forCellReuseIdentifier: "HealthFeedTableViewCell")
        self.tableViewHealthFeed.tableFooterView = UIView()
    }
    
    @objc func addHealthFeed(){
        
        self.push(id: Storyboard.Ids.CreateHealthFeedViewController, animation: true)
    }
    
    func setDesign() {
        self.tableViewHealthFeed.separatorStyle = .none
    }
}

//MARK:- TABLEVIEW DELEGATES AND DATASOURCES

@available(iOS 13.0, *)
extension HealthFeedViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.healthfeed.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "HealthFeedTableViewCell") as! HealthFeedTableViewCell
        cell.selectionStyle = .none
        self.setupData(cell: cell, data: self.healthfeed[indexPath.row])
        
        return cell
    }

    func setupData(cell : HealthFeedTableViewCell , data : healthResponseData){
        cell.Articlecontent.text = data.article_image?.first?.description ?? ""
        cell.publishedOrnotImg.image = (data.status != "Published") ? UIImage(named: "waiting") : UIImage(named: "published")
        cell.publishedOrnotImg.isHidden = false
        cell.ArticleTitle.text = data.title ?? ""
        guard let url = URL(string: data.article_image?.first?.image ?? "")else{
            return
        }
        DispatchQueue.main.async { [weak self] in
            let data = try? Data(contentsOf:(url))
            cell.ArticleImage.image = UIImage(data: data!)
                        }
                   
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.HealthFeedDetailsViewController) as! HealthFeedDetailsViewController
//        vc.healthfeed = self.healthfeed
        vc.titleText = self.healthfeed.first?.title ?? ""
        vc.imagetitle = self.healthfeed.first?.article_image?[indexPath.row].image ?? ""
        vc.descriptionText = self.healthfeed.first?.article_image?[indexPath.row].description ?? ""
        vc.timeText = dateConvertor(self.healthfeed.first?.article_image?[indexPath.row].created_at ?? "", _input: .date_time, _output: .DM)
        self.navigationController?.pushViewController(vc, animated: true)
    }


    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        let label = UILabel(frame: CGRect(x: 16, y: 0, width: self.view.frame.width, height: 40))
        label.text = Constants.string.publishedArticles.localize()
        headerView.backgroundColor = .white
        label.textColor = .black
        headerView.addSubview(label)
        Common.setFontWithType(to: label, size: 18.0, type: .meduim)
        return headerView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return 50
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 225
    }



}

//Api calls
@available(iOS 13.0, *)
extension HealthFeedViewController : DoctorPresenterToDoctorViewProtocol{
    func gethealthfeed(data: HealthFeedmodel) {
        self.healthfeed = data.responseData ?? []
    }
    

}


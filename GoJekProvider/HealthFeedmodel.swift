//
//  HealthFeedmodel.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 26/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct HealthFeedmodel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [healthResponseData]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct healthResponseData : Mappable {
    var id : Int?
    var title : String?
    var status : String?
    var article_image : [Article_image]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        title <- map["title"]
        status <- map["status"]
        article_image <- map["article_image"]
    }

}
struct Article_image : Mappable {
    var id : Int?
    var article_id : Int?
    var description : String?
    var image : String?
    var created_at : String?
    var days : String?
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        article_id <- map["article_id"]
        description <- map["description"]
        image <- map["image"]
        created_at <- map["created_at"]
        days <- map["days"]
    }

}
//struct requestarticle : Mappable {
//    var statusCode : String?
//    var title : String?
//    var message : String?
//    var responseData : [String]?
//    var error : [String]?
//
//    init?(map: Map) {
//
//    }
//
//    mutating func mapping(map: Map) {
//
//        statusCode <- map["statusCode"]
//        title <- map["title"]
//        message <- map["message"]
//        responseData <- map["responseData"]
//        error <- map["error"]
//    }
//
//}

struct ArticleReq : Mappable{
    var title : String = ""
    var description : String = ""
    var image : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        title <- map["title"]
        description <- map["description"]
        image <- map["cover_photo"]
    }
    
   
}

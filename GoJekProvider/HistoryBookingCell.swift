//
//  HistoryBookingCell.swift
//  GoJekProvider
//
//  Created by Shyamala's MacBook Pro on 07/09/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class HistoryBookingCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl : UILabel!
    @IBOutlet weak var timeLbl : UILabel!
    @IBOutlet weak var doctNameLbl : UILabel!
    @IBOutlet weak var statusLbl : UILabel!
    @IBOutlet weak var statusview : UIView!
    @IBOutlet weak var costLbl : UILabel!
    @IBOutlet weak var callBtn : UIButton!
    @IBOutlet weak var profileImg : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.callBtn.cornerRadius = self.callBtn.frame.width / 2
        self.profileImg.cornerRadius = self.profileImg.frame.width / 2
        self.statusview.cornerRadius = self.statusview.frame.height / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

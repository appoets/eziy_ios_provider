//
//  LanguageSelectionController.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 25/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper

class LanguageSelectionController: UIViewController {

    
    @IBOutlet weak var selectLanguageLbl: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var selectButton: UIButton!
    
    var isFromLaunch = true
    var selectedLanguageIndex = 0
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    private var selectedLanguage: Language = .english {
        didSet {
            LocalizeManager.share.setLocalization(language: selectedLanguage)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let languageStr = UserDefaults.standard.value(forKey: Constants.string.language) as? String,  let language = Language(rawValue: languageStr) {
            selectedLanguageIndex = Language.allCases.firstIndex(of: language) ?? 0
        }
        else {
            selectedLanguageIndex = 0
            selectedLanguage = .english
            UserDefaults.standard.set(self.selectedLanguage.rawValue, forKey: Constants.string.language)
        }
        
        self.selectButton.isHidden = false
        if !isFromLaunch {
            self.selectButton.isHidden = true
        }
        
        self.selectLanguageLbl.text = Constants.string.selectLanguage
        self.selectButton.setTitle(Constants.string.continueTxt, for: .normal)
        self.selectButton.addTarget(self, action: #selector(continueTapped), for: .touchUpInside)
        
        setLanguages()
    }
    
    @objc func continueTapped() {
        UserDefaults.standard.setValue(true, forKey: "First_Installed")
        self.push(id: Storyboard.Ids.LaunchViewController, animation: true)
    }
    
    @objc func viewTapped(_ sender : UITapGestureRecognizer) {
        self.selectedLanguageIndex = sender.view?.tag ?? 0
        let language = Language.allCases[selectedLanguageIndex]
       // guard language != self.selectedLanguage else {return}
        self.selectedLanguage = language
        
        UserDefaults.standard.set(self.selectedLanguage.rawValue, forKey: Constants.string.language)
        setLanguages()
        
        if !isFromLaunch {
//            updatelanguage(code: self.selectedLanguage.rawValue)
        }
        
    }
    
    func setLanguages() {
        for view in self.stackView.subviews {
            let viewGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
            viewGesture.view?.tag = view.tag
            view.addGestureRecognizer(viewGesture)
            
            for components in view.subviews {
                if let innerView = components.subviews.first {
                    innerView.isHidden = true
                    if self.selectedLanguageIndex == view.tag {
                        innerView.isHidden = false
                    }
                }
                
                if let titleLbl = components as? UILabel {
                    titleLbl.text = Language.allCases[view.tag].title
                }
                
            }
            
        }
        
    }
    
//    private func updatelanguage(code: String) {
//        self.presenter?.HITAPI(api: Base.updateLanguage.rawValue, params: ["language": code, "doctor_id": UserDefaultConfig.UserID], methodType: .POST, modelClass: LanguageEntity.self, token: true)
//    }
    
    

}

extension LanguageSelectionController : DoctorPresenterToDoctorViewProtocol {
    func showSuccess(api: String, dataArray: [Mappable]?, dataDict: Mappable?, modelClass: Any) {
        let navigationController = DoctorRouter.createdoctorModule()
        appDelegate.window?.rootViewController = navigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
//    func showError(error: CustomError) {
//        showToast(msg: error.localizedDescription)
//        //self.loader.isHideInMainThread(true)
//    }
    
    
}


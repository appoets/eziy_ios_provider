//
//  revenuemodel.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 09/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct revenuemodel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : RevenueResponseData?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct RevenueResponseData : Mappable {
    
    var total_paid_revenue : Int?
    var pending_revenue : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        total_paid_revenue <- map["total_paid_revenue"]
        pending_revenue <- map["pending_revenue"]
    }

}

struct DeletePatient : Mappable{
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : RevenueResponseData?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
}

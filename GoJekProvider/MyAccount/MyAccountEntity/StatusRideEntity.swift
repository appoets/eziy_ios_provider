//
//  StatusRideEntity.swift
//  GoJekProvider
//
//  Created by Sudar vizhi on 12/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper


struct StatusRideEntity : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [String]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
}



struct CategoryListModel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [CategoryDataModel]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
}

struct CategoryDataModel : Mappable {
    var id : Int?
    var service_category_id : Int?
    var service_subcategory_name : String?
    var picture : String?
    var service_subcategory_order : Int?
    var service_subcategory_status : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        service_category_id <- map["service_category_id"]
        service_subcategory_name <- map["service_subcategory_name"]
        picture <- map["picture"]
        service_subcategory_order <- map["service_subcategory_order"]
        service_subcategory_status <- map["service_subcategory_status"]
    }
}






struct DaySlotListModel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [DaySlotDataModel]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
}

struct DaySlotDataModel : Mappable {
    var id : Int?
    var day : String?
    var day_value : String?
    var is_selected : Bool?
    var time_slot : [TimeSlotDataModel]?


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        day <- map["day"]
        day_value <- map["day_value"]
        is_selected <- map["is_selected"]
        time_slot <- map["time_slot"]
    }
}


struct TimeSlotDataModel : Mappable {
    var id : Int?
    var company_id : Int?
    var start_time : String?
    var end_time : String?
    var time_selected : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        company_id <- map["company_id"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        time_selected <- map["time_selected"]
    }
}


struct AddDoctorDetail : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : AddDoctorDetailDataModel?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }
}

struct AddDoctorDetailDataModel : Mappable {
   
    var doctorProfile : DoctorProfile?
    

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        doctorProfile <- map["doctorProfile"]
        
    }
}


struct DoctorProfileModel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [DaySlotDataModel]?
    var responseDoctor : DoctorProfileDataModel?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        responseDoctor <- map["responseData"]
        error <- map["error"]
    }
}



struct DoctorProfileDataModel : Mappable {
    var provider_id : Int?
    var company_id : Int?
    var qualification : String?
    var experience : String?
    var specialist : String?
    var specializations : String?
    var location : String?
    var dob : String?
    var status : String?
    var consultation_fee : Int?
    var schedule_date : String?
    var picture : String?
    var medical_picture : String?
    var clinic_photos : String?
    var id : Int?
    var service : serviceResponseData?
    var provider : Provider?
    var time_slot : [ProviderTimeSlotModel]?
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        provider_id <- map["provider_id"]
        company_id <- map["company_id"]
        qualification <- map["qualification"]
        experience <- map["experience"]
        specialist <- map["specialist"]
        service <- map["service"]
        specializations <- map["specializations"]
        location <- map["location"]
        dob <- map["dob"]
        status <- map["status"]
        consultation_fee <- map["consultation_fee"]
        schedule_date <- map["schedule_date"]
        picture <- map["picture"]
        medical_picture <- map["medical_picture"]
        clinic_photos <- map["clinic_photos"]
        id <- map["id"]
        provider <- map["provider"]
        time_slot <- map["time_slot"]
    }

}



struct ProviderTimeSlotModel : Mappable {
    var id : Int?
    var doctor_profile_id : Int?
    var provider_id : Int?
    var working_day : String?
    var time_slot : Int?
    var slots_available_status : Int?
    var timing : [TimeSlotDataModel]?
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        doctor_profile_id <- map["doctor_profile_id"]
        provider_id <- map["provider_id"]
        working_day <- map["working_day"]
        time_slot <- map["time_slot"]
        timing <- map["timing"]
        slots_available_status <- map["slots_available_status"]
    }
}

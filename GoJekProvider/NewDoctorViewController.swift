//
//  NewDoctorViewController.swift
//  GoJekProvider
//
//  Created by Shyamala's MacBook Pro on 06/09/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class NewDoctorViewController: UIViewController {
    
    @IBOutlet weak var genderTxf : CustomTextField!
    @IBOutlet weak var qualificationTxf : CustomTextField!
    @IBOutlet weak var specialistTxf : CustomTextField!
    @IBOutlet weak var genderBtn : UIButton!
    @IBOutlet weak var dobTxt : CustomTextField!
    @IBOutlet weak var dobBtn : UIButton!
    @IBOutlet weak var serviceTxt : CustomTextField!
    @IBOutlet weak var specializationTxt : CustomTextField!
    @IBOutlet weak var locationTxt : CustomTextField!
    @IBOutlet weak var consultationfee : CustomTextField!
    @IBOutlet weak var saveBtn : UIButton!
    @IBOutlet weak var serviceBtn : UIButton!
    
    @IBOutlet weak var profileImgView : UIView!
    @IBOutlet weak var profileImgLbl : UILabel!
    @IBOutlet weak var profileImg : UIImageView!
    @IBOutlet weak var medicalCerLbl : UILabel!
    @IBOutlet weak var medicalCerImgView : UIView!
    @IBOutlet weak var medicalCerImg : UIImageView!
    @IBOutlet weak var clinicPhotoLbl : UILabel!
    @IBOutlet weak var clinicPhotoView : UIView!
    @IBOutlet weak var clinicPhotoImg : UIImageView!
    @IBOutlet weak var dayslotLbl : UILabel!
    @IBOutlet weak var dayslotView : UIView!
    @IBOutlet weak var dayslotcollection : UICollectionView!
    @IBOutlet weak var timeslotLbl : UILabel!
    @IBOutlet weak var timeslotView : UIView!
    @IBOutlet weak var timeslotcollection : UICollectionView!
    
    var profileImageupdate : Bool = false
    var medicalCerUpdate : Bool = false
    var clinicPhotoUpdate : Bool = false
    var categoryList : [CategoryDataModel]?
    var daySlot : [DaySlotDataModel]?
    var timeSlot : [TimeSlotDataModel]?
    var serviceID : Int?
    var doctorID : Int?
    var selectedDate : String = ""{
        didSet{
//            self.timeSlot = self.daySlot?.filter{$0.day_value == selectedDate}.first?.time_slot
            self.timeslotcollection.reloadData()
        }
    }
    
    var isEdit : Bool = false
    
    var gender : [String] = [Constants.string.male.localize(),Constants.string.female.localize(),Constants.string.others.localize()]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Add Doctor Details"
        self.setupView()
        self.setupCollection()
        self.setupAction()
        
        self.specializationList()
        
    }
    
    func setupView(){
        self.genderBtn.setTitle("", for: .normal)
        self.genderTxf.placeholder = "Gender"
        genderTxf.font = .setCustomFont(name: .light, size: .x16)
        self.qualificationTxf.placeholder = "Qualification"
        qualificationTxf.font = .setCustomFont(name: .light, size: .x16)
        self.specialistTxf.placeholder = "Specialist"
        qualificationTxf.font = .setCustomFont(name: .light, size: .x16)
        self.dobBtn.setTitle("", for: .normal)
        self.dobTxt.placeholder = "Date of Birth"
        dobTxt.font = .setCustomFont(name: .light, size: .x16)
        self.serviceBtn.setTitle("", for: .normal)
        self.serviceTxt.placeholder = "Service"
        serviceTxt.font = .setCustomFont(name: .light, size: .x16)
        self.specializationTxt.placeholder = "Specialization"
        serviceTxt.font = .setCustomFont(name: .light, size: .x16)
        self.locationTxt.placeholder = "Location"
        serviceTxt.font = .setCustomFont(name: .light, size: .x16)
        saveBtn.titleLabel?.font = .setCustomFont(name: .bold, size: .x16)
        clinicPhotoLbl.font = .setCustomFont(name: .bold, size: .x16)
        timeslotLbl.font = .setCustomFont(name: .bold, size: .x16)
        dayslotLbl.font = .setCustomFont(name: .bold, size: .x16)
        medicalCerLbl.font = .setCustomFont(name: .bold, size: .x16)
        profileImgLbl.font = .setCustomFont(name: .bold, size: .x16)
    }
    
    
    func setupAction(){
        self.profileImg.addTap {
            self.showImage(isRemoveNeed: nil, with:{ [weak self] (image) in
                self?.profileImageupdate = true
                self?.profileImg.image = image
            })
        }
        self.medicalCerImg.addTap {
            self.showImage(isRemoveNeed: nil, with:{ [weak self] (image) in
                self?.medicalCerUpdate = true
                self?.medicalCerImg.image = image
            })
        }
        self.clinicPhotoImg.addTap {
            self.showImage(isRemoveNeed: nil, with:{ [weak self] (image) in
                self?.clinicPhotoUpdate = true
                self?.clinicPhotoImg.image = image
            })
        }
        
        self.genderBtn.addTap {
            PickerManager.shared.showPicker(pickerData: self.gender, selectedData: "", completionHandler: { selectedData in
                
                self.genderTxf.text = selectedData
            })
        }
        
        self.serviceBtn.addTap {
            let category : [String] = self.categoryList?.map({$0.service_subcategory_name}) as! [String]
            PickerManager.shared.showPicker(pickerData: category, selectedData: "", completionHandler: { selectedData in
                self.serviceID = self.categoryList?.filter{$0.service_subcategory_name == selectedData}.first?.id ?? 0
                self.serviceTxt.text = selectedData
            })
        }
        
        self.saveBtn.addTap {
            self.addDoctorDetail()
        }
    }

  

}


extension NewDoctorViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == timeslotcollection{
            return self.daySlot?.filter{$0.day_value == selectedDate}.first?.time_slot?.count ?? 0
        }else{
            return self.daySlot?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == timeslotcollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "daytTmeSlotcell", for: indexPath) as! daytTmeSlotcell
            cell.daytimeLbl.text = self.daySlot?.filter{$0.day_value == selectedDate}.first?.time_slot?[indexPath.row].start_time ?? ""
            if (self.daySlot?.filter{$0.day_value == selectedDate}.first?.time_slot?[indexPath.row].time_selected ?? false){
                cell.daytimeView.backgroundColor = .lightGray
            }else{
                cell.daytimeView.backgroundColor = .white
            }
            cell.daytimeView.addTap {
                if (self.daySlot?.filter{$0.day_value == self.selectedDate}.first?.time_slot?[indexPath.row].time_selected ?? false ){
                    for i in (self.daySlot ?? [DaySlotDataModel]()).enumerated(){
                        if i.element.day_value == self.selectedDate{
                            self.daySlot?[i.offset].time_slot?[indexPath.row].time_selected = false
                            cell.daytimeView.backgroundColor = .white
                        }
                    }
                }else{
                    for i in (self.daySlot ?? [DaySlotDataModel]()).enumerated(){
                        if i.element.day_value == self.selectedDate{
                            self.daySlot?[i.offset].time_slot?[indexPath.row].time_selected = true
                            cell.daytimeView.backgroundColor = .lightGray
                        }
                    }
                }
               
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "daytTmeSlotcell", for: indexPath) as! daytTmeSlotcell
            cell.daytimeLbl.text = self.daySlot?[indexPath.row].day_value ?? ""
            
            if (self.daySlot?[indexPath.row].is_selected ?? false){
                cell.daytimeView.backgroundColor = .AppBlueColor.withAlphaComponent(0.5)
            }else{
                cell.daytimeView.backgroundColor = .white
            }
            
            cell.daytimeView.addTap {
                cell.daytimeView.backgroundColor = .lightGray
                self.selectedDate = self.daySlot?[indexPath.row].day_value ?? ""
                for i in (self.daySlot ?? [DaySlotDataModel]()).enumerated(){
                    if i.element.day_value == self.selectedDate{
                        self.daySlot?[i.offset].is_selected = true
                      //  cell.daytimeView.backgroundColor = .lightGray
                    }
                }
            }
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == timeslotcollection{
            return CGSize(width: 100, height: 100)
        }else{
            return CGSize(width: 100, height: 100)
        }
    }
    
    func setupCollection(){
        self.timeslotcollection.delegate = self
        self.timeslotcollection.dataSource = self
        self.dayslotcollection.delegate = self
        self.dayslotcollection.dataSource = self
        
        self.dayslotcollection.register(UINib(nibName: "daytTmeSlotcell", bundle: nibBundle), forCellWithReuseIdentifier: "daytTmeSlotcell")
        self.timeslotcollection.register(UINib(nibName: "daytTmeSlotcell", bundle: nibBundle), forCellWithReuseIdentifier: "daytTmeSlotcell")
    }
}

extension NewDoctorViewController : MyAccountPresenterToMyAccountViewProtocol{
    
    func specializationList(){
        self.myAccountPresenter?.getCategory()
        self.myAccountPresenter?.getDaySlot()
        self.myAccountPresenter?.getDoctorProfile(id: AppManager.share.getUserDetails()?.id ?? 0)
    }
   
    func getDoctCategorySuccess(category: CategoryListModel) {
        self.categoryList = category.responseData
    }
    
    func getDoctDaySuccess(category: DaySlotListModel) {
        self.daySlot = category.responseData
        self.selectedDate = self.daySlot?.first?.day_value ?? ""
        self.dayslotcollection.reloadData()
    }
    
    func getDoctoProfileDetailSuccess(profile: DoctorProfileModel) {
        if let response = profile.responseData{
            if (profile.responseData?.count ?? 0) == 0{
                isEdit = false
                
            }
        }else{
            if let responseDoctor = profile.responseDoctor{
                   isEdit = true
                self.saveBtn.setTitle("Update", for: .normal)
               
                self.updateData(profile: responseDoctor)
            }
        }
    }
    
    func updateData(profile : DoctorProfileDataModel){
        self.doctorID = profile.id ?? 0
        self.genderTxf.text = "Male"
        self.qualificationTxf.text = profile.qualification ?? ""
        self.specialistTxf.text = profile.specialist ?? ""
        self.serviceTxt.text = profile.service?.service_subcategory_name ?? ""
        self.serviceID = profile.service?.id ?? 0
        self.specializationTxt.text = profile.specializations ?? ""
        self.locationTxt.text = profile.location ?? ""
        self.consultationfee.text = "\(profile.consultation_fee ?? 0)"
        self.profileImg.sd_setImage(with: URL(string:  profile.picture ?? ""), placeholderImage:UIImage(named: Constant.userPlaceholderImage),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if (error != nil) {
                // Failed to load image
                self.profileImg.image = UIImage(named: Constant.profile)
                self.profileImageupdate = false
            } else {
                // Successful in loading image
                self.profileImg.image = image
                self.profileImageupdate = true
            }
        })
        self.medicalCerImg.sd_setImage(with: URL(string:  profile.medical_picture ?? ""), placeholderImage:UIImage(named: Constant.userPlaceholderImage),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if (error != nil) {
                // Failed to load image
                self.medicalCerImg.image = UIImage(named: Constant.profile)
                self.medicalCerUpdate = false
            } else {
                // Successful in loading image
                self.medicalCerImg.image = image
                self.medicalCerUpdate = true
            }
        })
        self.clinicPhotoImg.sd_setImage(with: URL(string:  profile.clinic_photos ?? ""), placeholderImage:UIImage(named: Constant.userPlaceholderImage),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
            // Perform operation.
            if (error != nil) {
                // Failed to load image
                self.clinicPhotoImg.image = UIImage(named: Constant.profile)
                self.clinicPhotoUpdate = false
            } else {
                // Successful in loading image
                self.clinicPhotoImg.image = image
                self.clinicPhotoUpdate = true
            }
        })
        
        
        for i in (self.daySlot ?? [DaySlotDataModel]()).enumerated(){
            for j in (profile.time_slot ?? [ProviderTimeSlotModel]()){
                if (i.element.day?.capitalized ?? "") == (j.working_day?.capitalized ?? ""){
                    self.daySlot?[i.offset].is_selected = true
                    
                    for timeI in (self.daySlot?[i.offset].time_slot ?? [TimeSlotDataModel]()).enumerated(){
                        if (timeI.element.start_time) == (j.timing?.first?.start_time ?? ""){
                            self.daySlot?[i.offset].time_slot?[timeI.offset].time_selected = true
                        }else{
                            self.daySlot?[i.offset].time_slot?[timeI.offset].time_selected = false
                        }
                    }
                }else{
                    self.daySlot?[i.offset].is_selected = false
                }
            }
        }
        
        self.timeslotcollection.reloadData()
        self.dayslotcollection.reloadData()
    }
    
    func addDoctorDetail(){
        var param : Parameters
        var imageData : [String : Data] = [String : Data]()
        if validation(){
            param = ["qualification" : self.qualificationTxf.text ?? "",
                     "specialist" : self.specialistTxf.text ?? "",
                     "dob" : "",
                     "specializations" : self.specializationTxt.text ?? "",
                     "location" : self.locationTxt.text ?? "",
                     "experience" : "",
                     "schedule_date" : "",
                     "consultation_fee" : self.consultationfee.text ?? "0",
                     "service" : self.serviceID ?? 0,
                     "admin_service" : "SERVICE"]
            if isEdit{
                param.updateValue("PATCH", forKey: "_method")
            }
            DispatchQueue.global().sync {
               for i in 0..<(self.daySlot?.count ?? 0){
                    if (self.daySlot?[i].is_selected ?? false) == true{
                    var index = 0
                    for j in (self.daySlot?[i].time_slot ?? [TimeSlotDataModel]()).enumerated(){
                        if j.element.time_selected == true{
                            param.updateValue(j.element.id ?? 0, forKey: "slot[\(self.daySlot?[i].day?.description ?? "")][\(index)]")
                            index += 1
                        }
                    }
                 }
              }
            }
           
            var profileImageData: Data!
            if  let profileData = profileImg.image?.jpegData(compressionQuality: 0.5) {
                profileImageData = profileData
            }
           
            var medicalCerImgData: Data!
            if  let medicalCerData = medicalCerImg.image?.jpegData(compressionQuality: 0.5) {
                medicalCerImgData = medicalCerData
            }
            
            var pictureData: Data!
            if  let pData = clinicPhotoImg.image?.jpegData(compressionQuality: 0.5) {
                pictureData = pData
            }
            
            imageData["picture"] = profileImageData
            imageData["medical_picture"] = medicalCerImgData
            imageData["clinic_photos"] = pictureData
            if isEdit{
                myAccountPresenter?.updateDoctData(isedit : true,id : self.doctorID ?? 0,param: param, imageData: imageData)
            }else{
                myAccountPresenter?.updateDoctData(isedit : false,id : 0,param: param, imageData: imageData)
            }
            
            
        }
    }
    
    func getDoctUpdateSuccess(category: AddDoctorDetail) {
        simpleAlert(view: self, title: String.Empty.localized, message: category.message ?? "",state: .success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func validation() -> Bool{
        guard !(genderTxf.text?.isEmpty)! else {
            simpleAlert(view: self, title: String.Empty.localized, message: "Select Gender Detail",state: .error)
            return false
        }
        guard !(qualificationTxf.text?.isEmpty)! else {
            qualificationTxf.becomeFirstResponder()
            simpleAlert(view: self, title: String.Empty.localized, message: "Enter Your Qualification",state: .error)
            return false
        }
        guard !(specialistTxf.text?.isEmpty)! else {
            specialistTxf.becomeFirstResponder()
            simpleAlert(view: self, title: String.Empty.localized, message: "Enter Specialist",state: .error)
            return false
        }
        guard !(consultationfee.text?.isEmpty)! else {
            consultationfee.becomeFirstResponder()
            simpleAlert(view: self, title: String.Empty.localized, message: "Enter Consultation fee",state: .error)
            return false
        }
        
        if !profileImageupdate{
            simpleAlert(view: self, title: String.Empty.localized, message: "Update Doctor Profile Image",state: .error)
            return false
        }
        
        if !medicalCerUpdate{
            simpleAlert(view: self, title: String.Empty.localized, message: "Update Medical Certificate Image",state: .error)
            return false
        }
        
        guard !(serviceTxt.text?.isEmpty)! else {
            
            simpleAlert(view: self, title: String.Empty.localized, message: "Select Service Type",state: .error)
            return false
        }
        
        guard !(specializationTxt.text?.isEmpty)! else {
            specializationTxt.becomeFirstResponder()
            simpleAlert(view: self, title: String.Empty.localized, message: "Enter Specialization",state: .error)
            return false
        }
        
        guard !(locationTxt.text?.isEmpty)! else {
            specializationTxt.becomeFirstResponder()
            simpleAlert(view: self, title: String.Empty.localized, message: "Enter clinic Location",state: .error)
            return false
        }
        
        if !clinicPhotoUpdate{
            simpleAlert(view: self, title: String.Empty.localized, message: "Update Clinic Photos",state: .error)
            return false
        }
      
        return true
    }
   
}

//
//  OrderHistoryVC.swift
//  GoJekProvider
//
//  Created by Shyamala's MacBook Pro on 07/09/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class OrderHistoryVC: UIViewController {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var historyList: UITableView!
    
    var historyType : String = ""{
        didSet{
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.historyList.delegate = self
        self.historyList.dataSource = self
        self.historyList.register(UINib(nibName: "HistoryBookingCell", bundle: nil), forCellReuseIdentifier: "HistoryBookingCell")
    }
    


    @IBAction func segmentAct(_ sender: Any) {
        
        if segmentedControl.selectedSegmentIndex == 0{
            historyType = "upcoming"
        }else{
            historyType = "past"
        }
    }
}

extension OrderHistoryVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryBookingCell", for: indexPath) as! HistoryBookingCell
        return cell
    }
    
    
}

extension OrderHistoryVC : OrdersPresenterToOrdersViewProtocol{
    
}

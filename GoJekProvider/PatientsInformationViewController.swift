//
//  PatientsInformationViewController.swift
//  GoJekProvider
//
//  Created by Navin's MacBook Pro on 05/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class PatientsInformationViewController: UIViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var labelPatientID: UILabel!
    @IBOutlet weak var labelPatientDetails: UILabel!
    @IBOutlet weak var buttonCharting: UIButton!
    @IBOutlet weak var buttonProfile: UIButton!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelStatusCenter: NSLayoutConstraint!
    @IBOutlet weak var chartingTable: UITableView!
    @IBOutlet weak var profileView: UIView!
    
    @IBOutlet weak var mobileNumberTitle: UILabel!
    @IBOutlet weak var mailTitle: UILabel!
    @IBOutlet weak var mailidView: UIView!
    @IBOutlet weak var mobileNumberView: UIView!
    @IBOutlet weak var patientIdView: UIView!
    @IBOutlet weak var patientNameView: UIView!
    @IBOutlet weak var profileDataView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var idTitle: UILabel!
    @IBOutlet weak var primaryMobile: UILabel!
    @IBOutlet weak var patientId: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    var horizontalConstraintCharting : NSLayoutConstraint!
    var horizontalConstraintProfile : NSLayoutConstraint!
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusLbl: UILabel!

    @IBOutlet weak var callBtn: UIButton!
    
    var PatientsArray : [PatientListData] = []
    var index = 0
    var firstname = String()
    var lastName = String()
    var id = String()
    var Image = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialLoads()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    self.populateData()
    }

}

extension PatientsInformationViewController {
    func initialLoads(){
        callBtn.cornerRadius = callBtn.frame.height / 2
        profileDataView.isHidden = true
        profileView.isHidden = true
        registerCell()
        print("PatientsArray.count",PatientsArray.count)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back").resizeImage(newWidth: 20), style: .plain, target: self, action: #selector(self.backButtonClick))

     //   self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(self.addAction))

        self.navigationItem.title = Constants.string.patientInformation.localize()
        self.buttonProfile.addTarget(self, action: #selector(profileAction), for: .touchUpInside)
        self.buttonCharting.addTarget(self, action: #selector(chartingAction), for: .touchUpInside)
        
        let data = self.PatientsArray[self.index]
        if ((data.status ?? "") == "CONSULTED") || ((data.status ?? "") == "CANCELLED"){
            self.callBtn.isHidden = true
            self.statusView.isHidden = false
            self.statusLbl.text = "You has CONSULTED"
            self.statusLbl.textColor = .blue
        }else{
            self.callBtn.isHidden = false
            self.statusView.isHidden = true
            self.statusLbl.text = "Meet has CANCELLED"
            self.statusLbl.textColor = .red
        }
        
        
        self.callBtn.addTap {
            let data = self.PatientsArray[self.index]
            let param : Parameters = ["room_id" :  "room_id_\(data.provider_id ?? 0)_\(data.booking_id ?? "0")_\(data.patient_id ?? 0)",
                                      "video" : "1",
                                      "hospital_id" :"\(data.provider_id ?? 0)",
                                      "patient_id" : "\(self.PatientsArray[self.index].patient?.id ?? 0)",
                                      "id" : "\(data.id ?? 0)",
                                      "push_to" : "patient"]
            self.homePresenter?.chatFiler(param: param)
            
           
            
        }
        
        

    }
    func registerCell(){
        
        self.chartingTable.tableFooterView = UIView()
        
        self.chartingTable.register(UINib(nibName: "ChartingTableViewCell", bundle: nil), forCellReuseIdentifier: "ChartingTableViewCell")

    }
    
    func initiateCall(){
       
    }
    
    func acceptRequestSuccess(acceptRequestEntity: HomeEntity) {
        showToast(msg: acceptRequestEntity.message ?? "")
        
        let JitsiCallVC = UIStoryboard(name: "Doctor", bundle: nil).instantiateViewController(identifier: "JitsiCallVC") as! JitsiCallVC
        JitsiCallVC.modalPresentationStyle = .fullScreen
        let data = self.PatientsArray[self.index]
        JitsiCallVC.patientId = "\(self.PatientsArray[self.index].patient?.id ?? 0)"
        JitsiCallVC.roomid =  "\(self.PatientsArray[self.index].provider?.id ?? 0)_video_\(self.PatientsArray[self.index].patient?.id ?? 0)"
        JitsiCallVC.roomid = "room_id_\(data.provider_id ?? 0)_\(data.booking_id ?? "0")_\(data.patient_id ?? 0)"
        JitsiCallVC.room_id = "room_id_\(data.provider_id ?? 0)_\(data.booking_id ?? "0")_\(data.patient_id ?? 0)"
        JitsiCallVC.patient_id = "\(data.patient_id ?? 0)"
        JitsiCallVC.id = "\(data.id ?? 0)"
        JitsiCallVC.hospital_id = "\(data.provider_id ?? 0)"
            self.present(JitsiCallVC, animated: true, completion: nil)
    }
    
    @objc func chartingAction() {
        chartingTable.isHidden = false
        profileView.isHidden = true
        profileDataView.isHidden = true
        self.buttonProfile.setTitleColor(UIColor.darkGray, for: .normal)
        self.buttonCharting.setTitleColor(UIColor.AppBlueColor, for: .normal)
        self.labelStatusCenter.constant = 0
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Add").resizeImage(newWidth: 20), style: .plain, target: self, action: #selector(self.addAction))
    //    self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(self.addAction))

        self.buttonProfile.setTitleColor(UIColor(named: "TextForegroundColor"), for: .normal)
        self.buttonCharting.setTitleColor(UIColor.AppBlueColor, for: .normal)

    }

    @objc func profileAction() {
       
        chartingTable.isHidden = true
        self.buttonProfile.setTitleColor(UIColor.AppBlueColor, for: .normal)
        self.buttonCharting.setTitleColor(UIColor(named: "TextForegroundColor"), for: .normal)
        if PatientsArray.count > 0 {
            profileData()
            profileDataView.isHidden = false
            profileView.isHidden = true
        }
        else {
        profileView.isHidden = false
            profileDataView.isHidden = true
        }
        self.labelStatusCenter.constant = self.buttonProfile.frame.origin.x
      //  self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: Constants.string.edit.localize(), style: .done, target: self, action: #selector(self.profileEditAction))

    }
    
    @objc func profileEditAction() {
//        let vc = EditPatientInformationTableViewController.initVC(storyBoardName: .main, vc: EditPatientInformationTableViewController.self, viewConrollerID: Storyboard.Ids.EditPatientInformationTableViewController)
//        vc.Patients = self.Patients
//        self.push(from: self, ToViewContorller: vc)
    }
    
    @objc func addAction() {
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.profileImg.makeRoundedCorner()
        self.profileImageView.makeRoundedCorner()

    }
    func populateData(){
    //    let data : Patient = self.PatientsArray
            self.labelName.text = "\(firstname ?? "")"
            self.labelPatientID.text = "\(id ?? "")"
          //  self.labelPatientDetails.text = "\(data.profile?.age ?? "") Year ,\(data.profile?.gender ?? "")"
        self.profileImg.image = UIImage(named: "profile")
    
        
        }
  
    func profileData() {
        self.nameLabel.text = PatientsArray[index].patient?.patient_name ?? ""
        self.patientId.text = "\(PatientsArray[index].patient?.id ?? 0)"
        self.primaryMobile.text = "\(PatientsArray[index].patient?.patient_contact_number ?? "0")"
        self.emailLabel.text = PatientsArray[index].patient?.patient_email ?? ""
        
        self.profileImageView.image = UIImage(named: "profile")
        
        
    }
}

extension PatientsInformationViewController : HomePresenterToHomeViewProtocol{
    
}

extension PatientsInformationViewController : UITableViewDelegate,UITableViewDataSource {
    
   
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = chartingTable.dequeueReusableCell(withIdentifier: "ChartingTableViewCell", for: indexPath) as! ChartingTableViewCell
        self.setupData(cell: tableCell, data: self.PatientsArray[indexPath.row],index : indexPath.section)
        
      return tableCell
    }
    

    
    func setupData(cell : ChartingTableViewCell , data : PatientListData,index : Int){
        if index == 0{
            cell.labelAppointmentDetails.text = "\(data.patient?.patient_name ?? "")"
        }else{
            cell.labelAppointmentString.text = "Appointment time: "
            cell.labelAppointmentDetails.text = "\(data.consult_time ?? "")"
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(named: "TextBackgroudCOlor")
        let titleLabel = UILabel()
    //    titleLabel.text = "Scheduled - \(dateConvertor(self.Patients.appointments?[section].scheduled_at ?? "", _input: .date_time, _output: .edmy))"
        Common.setFontWithType(to: titleLabel, size: 14.0, type: .meduim)
        titleLabel.textColor = UIColor(named: "TextBlackColor")
        titleLabel.frame = CGRect(x: 16, y: headerView.frame.height/2, width: self.view.frame.width-16, height: 40)
        headerView.addSubview(titleLabel)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    

   

}

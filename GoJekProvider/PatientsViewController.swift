//
//  PatientsViewController.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 25/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import ObjectMapper
import DropDown

class PatientsViewController: UIViewController{
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var patientsTable: UITableView!
    var selectedDate : String = ""
    var isFromCalendar : Bool = false
  //  var Patient : PatientList?
    var patentListData : [PatientListData] = []
    let dropDown = DropDown()
//    var index : Int = 0
//    var all_appointments : [All_appointments] = [All_appointments]()
//    var appoinment : All_appointments = All_appointments()
//    var appointEntities : AppointEntities?
    
    var idd : Int = 0
    private lazy var loader  : UIView = {
        return createActivityIndicator(self.view)
    }()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialLoads()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.doctorPresenter?.getPatientlist()
    }
    func deletePatientDetail(id : String){
        print(id)
        self.doctorPresenter?.deletePatient(id: id)
        self.loader.isHidden = true
    }


}


extension PatientsViewController {
    func initialLoads(){
        registerCell()
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back").resizeImage(newWidth: 20), style: .plain, target: self, action: #selector(self.backButtonClick))

        self.navigationItem.title = Constants.string.patients.localize()
//        self.searchBar.delegate = self
//        self.getPatientsList()

    }
    func registerCell(){
        
        self.patientsTable.tableFooterView = UIView()
        self.patientsTable.register(UINib(nibName: "PatientTableViewCell", bundle: nil), forCellReuseIdentifier: "PatientTableViewCell")

    }
    
   @objc func showOptions(sender : UIButton){
       dropDown.show()
       dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
           
           if index == 0 {
               self.deletePatientDetail(id: String(self.patentListData[sender.tag].id ?? 0))
           }
           else {
              // self.deletePatientDetail(id: String(self.patentListData[sender.tag].id ?? 0))
           }
         print("Selected item: \(item) at index: \(index)")
       }

//        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: Constants.string.callPatient.localize(), style: .default, handler: { (_) in
//            if let url = URL(string: "tel://\(self.todayPatients[sender.tag].phone ?? "")"), UIApplication.shared.canOpenURL(url) {
//                if #available(iOS 10, *) {
//                    UIApplication.shared.open(url)
//                } else {
//                    UIApplication.shared.openURL(url)
//                }
//            }
//        }))
//
//        alert.addAction(UIAlertAction(title: Constants.string.addAppointment.localize(), style: .default, handler: { (_) in
//            print("User click Edit button")
//            if self.isFromCalendar{
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.CreateAppointmentViewController) as! CreateAppointmentViewController
//            vc.patientDetails = self.todayPatients[sender.tag]
//            vc.selectedDate = self.selectedDate
//                vc.isFromCalendar = self.isFromCalendar
//                vc.serviceId = Int(profile.doctor?.services_id ?? "0") ?? 0
//            self.navigationController?.pushViewController(vc, animated: true)
//            }else{
//                let view = DateTimePickerAlert.getView
////                view.alertdelegate = self
//                self.index = sender.tag
//                AlertBuilder().addView(fromVC: self , view).show()
//            }
//
//        }))
//
////        alert.addAction(UIAlertAction(title: "Add File", style: .default, handler: { (_) in
////            print("User click Edit button")
////        }))
//
//        alert.addAction(UIAlertAction(title:Constants.string.deletePatient.localize(), style: .destructive, handler: { (_) in
//            self.deletePatientDetail(id: self.todayPatients[sender.tag].id?.description ?? "")
//        }))
//
//        alert.addAction(UIAlertAction(title: Constants.string.dismiss.localize(), style: .cancel, handler: { (_) in
//            print("User click Dismiss button")
//        }))
//
//        self.present(alert, animated: true, completion: {
//            print("completion block")
//        })
  }
}
//
extension PatientsViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patentListData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = self.patientsTable.dequeueReusableCell(withIdentifier: "PatientTableViewCell", for: indexPath) as! PatientTableViewCell
    self.populateData(cell: tableCell, data: patentListData[indexPath.row])
    //    tableCell.labelName.text = "\(doctorDetail[indexPath.row].provider?.first_name ?? "") \(doctorDetail[indexPath.row].provider?.last_name ?? "")"
     //   print("doctorDetail[indexPath.row].provider?.first_name ??",doctorDetail[indexPath.row].provider?.first_name ?? "")
        dropDown.anchorView = tableCell.buttonOptions
        dropDown.dataSource = ["Delete"]
        dropDown.width = 150
     tableCell.buttonOptions.tag = indexPath.row
    tableCell.buttonOptions.addTarget(self, action: #selector(showOptions(sender:)), for: .touchUpInside)
        tableCell.selectionStyle = .none
        return tableCell
    }

    func populateData(cell : PatientTableViewCell, data : PatientListData){
        print("data.provider?.unique_id ??",data.provider?.unique_id ?? "")
        print("data.provider?.first_name ??",data.provider?.first_name ?? "")
        
        cell.labelName.text = "\(data.patient?.patient_name ?? "")"
//        cell.labelPatientDetails.text = "\(data.provider?.age ?? "") years , \(data.provider.profile?.gender ?? "")"
        cell.labelPatientID.text = data.provider?.unique_id ?? ""
//        "TELM\(data.regn_id ?? 0)"
      cell.imageView?.makeRoundedCorner()
     
        cell.patientImg?.image = UIImage(named: "profile")
        
        if (data.status ?? "") == "CONSULTED"{
            cell.buttonOptions.isHidden = true
        }else{
            cell.buttonOptions.isHidden = false
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.PatientsInformationViewController) as! PatientsInformationViewController
        vc.PatientsArray = self.patentListData
        vc.index = indexPath.row
        vc.id = patentListData[indexPath.row].patient?.unique_id ?? ""
        vc.firstname = patentListData[indexPath.row].patient?.patient_name ?? ""
      //  vc.lastName = patentListData[indexPath.row].patient?.last_name ?? ""
        vc.Image = "profile"
             //      vc.isFromPatient = true
                //   vc.allPatient = self.doctorDetail[indexPath.row]
                 //  vc.allPatientAppointments = self.doctorDetail[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
//        if isFromCalendar {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.CreateAppointmentViewController) as! CreateAppointmentViewController
//        vc.patientDetails = self.todayPatients[indexPath.row]
//        vc.selectedDate = self.selectedDate
//        vc.isFromCalendar = self.isFromCalendar
//        self.navigationController?.pushViewController(vc, animated: true)
//        }else{
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.PatientsInformationViewController) as! PatientsInformationViewController
//            vc.isFromPatient = true
//            vc.allPatient = self.todayPatients[indexPath.row]
//            vc.allPatientAppointments = self.todayPatients[indexPath.row].appointments
//            self.navigationController?.pushViewController(vc, animated: true)
//        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }


}

//Api calls
extension PatientsViewController : DoctorPresenterToDoctorViewProtocol{
   
    func deletePatient(data: DeletePatient) {
        self.loader.isHidden = true
        ToastManager.show(title: data.message ?? "", state: .success)
        self.doctorPresenter?.getPatientlist()
    }
    func getPatientlist(data: PatientList) {
        print("patientdat",data)
        self.patentListData = data.responseData?.data ?? []
        print("doctorDetail", self.patentListData.count)
        print("doctorDetail1", data.responseData?.data ?? [])
        
        DispatchQueue.main.async {
            self.patientsTable.reloadData()
        }
      
        
    }
    
}

extension PatientsViewController  : AlertDelegate{
    func selectedDate(selectionType: String, date: String, alertVC: UIViewController) {

    }

    func selectedDateTime(selectionType: DateselectionOption,date : Date, datestr: String, time: String, alertVC: UIViewController) {
       let vc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.CreateAppointmentViewController) as! CreateAppointmentViewController
        //    vc.patientDetails = self.todayPatients[self.index]
            vc.isFromCalendar = self.isFromCalendar
            let dateAsString =  datestr
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm a"
            let date = dateFormatter.date(from: dateAsString)

            dateFormatter.dateFormat = "HH:mm:ss"
            let date24 = dateFormatter.string(from: date ?? Date())

            vc.selectedDate = datestr + " " + date24
            self.navigationController?.pushViewController(vc, animated: true)
    }

    func selectedTime(time: String, alertVC: UIViewController) {

    }


}


//extension PatientsViewController : UISearchBarDelegate {
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        if !searchText.isEmpty{
//            let url = "\(Base.searchPatient.rawValue)?search=\(searchText)"
//            self.presenter?.HITAPI(api: url, params: nil, methodType: .GET, modelClass: SearchPatient.self, token: true)
//
//        }
//    }
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        self.getPatientsList()
//    }
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
//           print("end searching --> Close Keyboard")
//           self.searchBar.endEditing(true)
//       }
////
//
//}


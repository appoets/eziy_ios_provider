//
//  ProfileServiceViewcontroller.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 08/04/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit
import CLWeeklyCalendarView
import DropDown
import  Alamofire
protocol ScheduleSlotCellProtocol{
    func selected(slotDay:String,indexPath: IndexPath)
    func selectedSlot(slotTime:String, indexpath:IndexPath)

}
class ProfileServiceViewcontroller : UIViewController {
    

    @IBOutlet weak var genderTextfield:CustomTextField!
    @IBOutlet weak var qualificationTextfield:CustomTextField!
    @IBOutlet weak var specalistTextfield:CustomTextField!
    @IBOutlet weak var dobTextfield:CustomTextField!
    @IBOutlet weak var medicalceritificateimage: UIView!
    @IBOutlet weak var profilecoverimage: UIView!
    @IBOutlet weak var medicalcerificatelbl: UILabel!
    @IBOutlet weak var genderview: UIView!
    @IBOutlet weak var profilelbl: UILabel!
    @IBOutlet weak var calendarView: CLWeeklyCalendarView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dayslotlbl: UILabel!
    @IBOutlet weak var servicelbl: UILabel!
    @IBOutlet weak var specilaztionlbl: UILabel!
    @IBOutlet weak var profileimageview: UIImageView!
    @IBOutlet weak var locationlbl: UILabel!
    @IBOutlet weak var gender_btn: UIButton!
    @IBOutlet weak var servicetextfiled: HoshiTextField!
    @IBOutlet weak var dropdownbtn: UIButton!
    @IBOutlet weak var specializationtextfield: HoshiTextField!
    @IBOutlet weak var addmedicalphoto: UIButton!
    @IBOutlet weak var medicalimageview: UIImageView!
    @IBOutlet weak var locationtextfield: HoshiTextField!
    @IBOutlet weak var Clinilbl: UILabel!
    @IBOutlet weak var clinicview: UIView!
    @IBOutlet weak var clinicaddbtn: UIButton!
    @IBOutlet weak var addimagebtn: UIButton!
    @IBOutlet weak var ScheduleTimeCollectioview: UICollectionView!
    @IBOutlet weak var daycollectionview: UICollectionView!
    @IBOutlet weak var clinicimageview: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    var timeArr = [String]()
    var resData: profileaboutentity?
    var slots : [SlotResponseData] = []{
        didSet{
            daycollectionview.reloadInMainThread()
        }
    }
    var timeslot : [timeslotResponseData] = []{
        didSet{
            ScheduleTimeCollectioview.reloadInMainThread()
        }
    }
    var scheduleDate = Date()
    var scheduleDateString : String = String()
    var delegate: ScheduleSlotCellProtocol?
    var servicelist : servicemodel?
    var service = DropDown()
    var reasonTextData = ""
    var picker = UIPickerView()
    var selectedPickerValue = String()
//    var slotList = [String]()
    var slotPeriod = String()
    var slot = String()
//    var slotIndex : Int = -1
//    var isbooked:Bool = false
//    var bookedIndexpath:IndexPath!
//
    
    private lazy var loader  : UIView = {
        return createActivityIndicator(self.view)
        
    }()
    
    var gender : [String] = [Constants.string.male.localize(),Constants.string.female.localize(),Constants.string.others.localize()]
    
    override func viewDidLoad() {
            super.viewDidLoad()
        self.placeholder()
        self.initialload()
        saveBtnTapped()
       
       
        
        }
    override func viewWillAppear(_ animated: Bool) {
        self.doctorPresenter?.getservicelist()
        self.doctorPresenter?.getslotlist()
        self.doctorPresenter?.gettimelist()
        self.servicetextfiled.delegate = self
        self.genderTextfield.delegate = self
        daycollectionview.delegate = self
        daycollectionview.dataSource = self
        ScheduleTimeCollectioview.delegate = self
        ScheduleTimeCollectioview.dataSource = self
       
        
    }
    
    func saveBtnTapped(){
        self.saveButton.addTap { [self] in
//        if self.validation(){
            let param : Parameters  = ["qualification":self.qualificationTextfield.text ?? "",
                                       "Specialist":self.specializationtextfield.text ?? "","dob":self.dobTextfield?.text ?? 0,"schedule_date":self.dobTextfield.text ?? "" ,"specializations":self.specializationtextfield.text ?? "","location":self.locationtextfield.text ?? "",
                                       "service" : self.servicetextfiled.text ?? ""]
            
            var profileImageData: Data!
            if  let profileData = self.profileimageview.image?.jpegData(compressionQuality: 0.5) {
                profileImageData = profileData
            }
            var medicalImageData: Data!
            if let medicalData = self.medicalimageview.image?.jpegData(compressionQuality: 0.5){
                medicalImageData = medicalData
            }
            var clinicImageData : Data?
            if let clinic = self.clinicimageview.image?.jpegData(compressionQuality: 0.5){
                clinicImageData = clinic
            }
            self.doctorPresenter?.profiledata(param: param, imagedata: ["image" : profileImageData!])
//        }
            
    }
    }
  
    
    
    func initialload(){
        self.dayslotlbl.text = Constants.string.daySlot.localize()
        self.addimagebtn.addTarget(self, action: #selector(profileCoverPhoto), for: .touchUpInside)
        self.addmedicalphoto.addTarget(self, action: #selector(medicalCoverPhoto), for: .touchUpInside)
        self.clinicaddbtn.addTarget(self, action: #selector(clinicCoverPhoto), for: .touchUpInside)
        self.profileimageview.addTap {
            self.profileCoverPhoto()
        }
        self.medicalimageview.addTap {
            self.medicalCoverPhoto()
        }
        self.clinicimageview.addTap {
            self.clinicCoverPhoto()
        }
        
        }
    
    func validation()->Bool{
        if ((self.genderTextfield.text?.isEmpty) == nil){
            showToast(msg:ErrorMessage.list.gender)
            return false
        }else if ((self.qualificationTextfield.text?.isEmpty) == nil){
            showToast(msg:ErrorMessage.list.qualification)
            return false
        }else if ((self.qualificationTextfield.text?.isEmpty) == nil){
            showToast(msg:ErrorMessage.list.qualification)
            return false
        }else if ((self.specializationtextfield.text?.isEmpty) == nil){
            showToast(msg:ErrorMessage.list.speciality)
            return false
        }else if ((self.dobTextfield.text?.isEmpty) == nil){
            showToast(msg:ErrorMessage.list.dataOfBirth)
            return false
        }else if ((self.profileimageview.image) == nil){
            return false
        }else if ((self.medicalimageview.image) == nil){
            showToast(msg:ErrorMessage.list.medicalPhoto)
            return false
        }else if ((self.servicetextfiled.text?.isEmpty) == nil){
            showToast(msg:ErrorMessage.list.service)
            return false
        }else if ((self.specializationtextfield.text?.isEmpty) == nil){
            showToast(msg:ErrorMessage.list.specialization)
            return false
        }else if ((self.locationtextfield.text?.isEmpty) == nil){
            showToast(msg:ErrorMessage.list.location)
            return false
        }else if ((self.clinicimageview.image) == nil){
            showToast(msg:ErrorMessage.list.clinicPhoto)
            return false
        }else{
            return false
        }
        return false
    }

   
        
    func placeholder(){
            self.genderTextfield.placeholder = Constants.string.gender
            self.qualificationTextfield.placeholder = Constants.string.qualification
            self.specalistTextfield.placeholder = Constants.string.specilist
            self.dobTextfield.placeholder = Constants.string.dob
            self.profilelbl.text = Constants.string.profile
            self.medicalcerificatelbl.text = Constants.string.medicals
        self.servicelbl.text = Constants.string.Service
        self.specilaztionlbl.text = Constants.string.Specilization
        self.locationlbl.text = Constants.string.location
        self.Clinilbl.text = Constants.string.clinic
        
        }
    override func viewWillLayoutSubviews() {
       let frame = CGRect(x: 10, y: 10, width: self.view.frame.width - 20, height: 300)
       self.profilecoverimage.frame = frame
       self.medicalceritificateimage.frame = frame
       self.clinicview.frame = frame
        profilecoverimage.addShadow(offset: CGSize(width: 0, height: 0), color: .lightGray, radius: 2 , opacity: 0.4)
        profilecoverimage.cornerRadius = 20
        medicalceritificateimage.cornerRadius = 20
        clinicview.cornerRadius = 20
        medicalceritificateimage.addShadow(offset: CGSize(width: 0, height: 0), color: .lightGray, radius: 2 , opacity: 0.4)
        clinicview.addShadow(offset: CGSize(width: 0, height: 0), color: .lightGray, radius: 2 , opacity: 0.4)
    }
    @objc func profileCoverPhoto(){
        self.showImage(isRemoveNeed: "") { (image) in
            self.profileimageview.image = image
           
            self.clinicimageview.image = image
        }
    }
    @objc func medicalCoverPhoto(){
        self.showImage(isRemoveNeed: "") { (image) in
            self.medicalimageview.image = image
    }
    }
    @objc func clinicCoverPhoto(){
            self.showImage(isRemoveNeed: "") { (image) in
            self.clinicimageview.image = image
    }
    }

    }

class ScheduleTimeCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var timeslotlbl: UILabel!
    @IBOutlet weak var timeslotbaseview: UIView!
    

   override func awakeFromNib() {
        super.awakeFromNib()
//        Common.setFont(to: timeslotlbl, isTitle: false, size: 13)
    }
}
class dayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dayCollectionViewHeightConstrains: NSLayoutConstraint!
    @IBOutlet weak var baseviewce: UIView!
    @IBOutlet weak var daylbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        Common.setFont(to: daylbl, isTitle: false, size: 13)
    }
}




@available(iOS 13.0, *)
extension ProfileServiceViewcontroller : DoctorPresenterToDoctorViewProtocol{
    func profiledata(data: profileaboutentity) {
        self.resData = data
        ToastManager.show(title: "Successfully updated" , state: .success)
        self.navigationController?.popViewController(animated: true)
    }
    
    func getservicelist(data: servicemodel) {
        self.servicelist = data
    }
    
    func getslotlist(data: Slotmodel) {
        self.slots = data.responseData ?? []
        
    }
    
    func gettimelist(data: TimeSlotmodel) {
        self.timeslot = data.responseData ?? []
        
        for i in data.responseData ?? [] {
            self.timeArr.append(i.doctor_time_slot?.start_time ?? "")
            self.timeArr.append(i.doctor_time_slot?.end_time ?? "")
        }
       
        print("self.timeArr--->>",self.timeArr)
        print("timeslottimeslot",timeslot)
        
    }
    

}
extension ProfileServiceViewcontroller : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == servicetextfiled {
           service.anchorView = textField
            var dataSource:[String]=[]
            for serviceA in self.servicelist?.responseData ?? [] {
                print("hi")
                dataSource.append(serviceA.service_subcategory_name ?? "")
            }
            service.dataSource = dataSource
            service.selectionAction = { [weak self] (index, item) in
                self?.servicetextfiled.text = item
                self?.reasonTextData =  self?.servicetextfiled.text ?? ""
            }
            DropDown.setupDefaultAppearance()
         service.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
           service.show()
       
            return false
        }
        if textField == genderTextfield {
            PickerManager.shared.showPicker(pickerData: gender, selectedData: textField.text, completionHandler: { selectedData in
                textField.text = selectedData
                self.genderTextfield.text = selectedData
            })
       
            return false
        }
        return true
    }
}
extension ProfileServiceViewcontroller:UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.daycollectionview {
            return slots.count
        }else if collectionView == self.ScheduleTimeCollectioview{
            return timeslot.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.daycollectionview{
            let width = (collectionView.frame.width/5)
            return  CGSize(width: width, height: width)
        }else if collectionView == self.ScheduleTimeCollectioview{
            let width = (collectionView.frame.width/5)
            return  CGSize(width: width, height: width)
        }
        let width = (collectionView.frame.width)
        return  CGSize(width: width, height: width)
//        let width = (collectionView.frame.width/7)
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.daycollectionview{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dayCollectionViewCell", for: indexPath) as! dayCollectionViewCell
            let bgColorView = UIView()
            bgColorView.backgroundColor = .AppBlueColor
            cell.selectedBackgroundView = bgColorView
            bgColorView.cornerRadius = bgColorView.frame.height/2
            collectionView.layoutIfNeeded()
                            cell.contentView.layoutIfNeeded()
                            cell.baseviewce.layoutIfNeeded()
                            cell.baseviewce.layer.borderColor = UIColor.gray.cgColor
                            cell.baseviewce.layer.borderWidth = 1
                            cell.baseviewce.backgroundColor = .whiteColor
                            cell.baseviewce.layer.cornerRadius =  cell.baseviewce.frame.width/2
                            cell.daylbl.text = slots[indexPath.row].day_value
            return cell
           
        }else if  collectionView == self.ScheduleTimeCollectioview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScheduleTimeCollectionViewCell", for: indexPath) as! ScheduleTimeCollectionViewCell
            let bgColorView = UIView()
            bgColorView.backgroundColor = .AppBlueColor
            cell.selectedBackgroundView = bgColorView
            bgColorView.cornerRadius = bgColorView.frame.height/2
            collectionView.layoutIfNeeded()
            cell.timeslotbaseview.layoutIfNeeded()
            cell.timeslotbaseview.layer.borderColor = UIColor.gray.cgColor
            cell.timeslotbaseview.layer.borderWidth = 1
            cell.timeslotbaseview.backgroundColor = .whiteColor
            cell.timeslotbaseview.layer.cornerRadius =  cell.timeslotbaseview.frame.width/2
            cell.timeslotlbl.text = self.timeArr[indexPath.row]
                   return cell
        }
        return UICollectionViewCell()
        }
        
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath,_ date: Date!) {
       
       

        if collectionView == self.daycollectionview{
            let sections = (slot == "Mon") ? 0:1
            let bookingIndex = IndexPath(row:indexPath.row,section: sections)
            print("mnmnmnmn",bookingIndex)
            delegate?.selected(slotDay: slots[indexPath.row].day ?? "" , indexPath: bookingIndex)
            
            
            
        }else if collectionView == self.ScheduleTimeCollectioview{
            let section = (slotPeriod == "Mon") ? 0: 1
                        let bookedIndexpath =  IndexPath(row: indexPath.row, section: section)
                        print("asdfghjkqwertyu",bookedIndexpath,section)
                        delegate?.selectedSlot(slotTime: slots[indexPath.row].day_value ?? "", indexpath: bookedIndexpath)
        }
        
        
       
    }
}

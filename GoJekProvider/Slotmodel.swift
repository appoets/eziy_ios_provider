//
//  Slotmodel.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 11/04/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct Slotmodel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [SlotResponseData]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct SlotResponseData : Mappable {
    var id : Int?
    var day : String?
    var day_value : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        day <- map["day"]
        day_value <- map["day_value"]
    }

}

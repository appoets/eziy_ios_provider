//
//  CalendarTableViewCell.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 30/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class CalendarTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTreatment: UILabel!
    @IBOutlet weak var buttonOptions: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


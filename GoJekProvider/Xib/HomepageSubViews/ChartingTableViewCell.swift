//
//  ChartingTableViewCell.swift
//  GoJekProvider
//
//  Created by Navin's MacBook Pro on 05/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class ChartingTableViewCell: UITableViewCell {

    @IBOutlet weak var labelAppointmentString: UILabel!
    @IBOutlet weak var labelAppointmentDetails: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  HealthFeedTableViewCell.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 26/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class HealthFeedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ArticleImage : UIImageView!
    @IBOutlet weak var ArticleTitle : UILabel!
    @IBOutlet weak var Articlecontent : UILabel!
    @IBOutlet weak var publishedDate : UILabel!
    @IBOutlet weak var shadowView : UIView!
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var publishedOrnotImg : UIImageView!
    
    var publishedValue : Int = 0{

        didSet{

            publishedOrnotImg.image = publishedValue == 0 ? UIImage(named: "waiting") : UIImage(named: "published")
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.shadowView.addShadow(offset: CGSize(width: 1.0,height: 1.0), color: .lightGray, radius: 2, opacity: 0.4)
        self.bgView.cornerRadius = 10.0
    }
}


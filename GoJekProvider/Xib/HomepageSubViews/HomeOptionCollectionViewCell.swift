//
//  HomeOptionCollectionViewCell.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 19/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class HomeOptionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var outerview: UIView!
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setdesign()
        // Initialization code
    }
    func setdesign(){
    
        outerview.addShadow(offset: CGSize(width: 0, height: 1), color: .lightGray, radius: 2, opacity: 0.65)
    }

}


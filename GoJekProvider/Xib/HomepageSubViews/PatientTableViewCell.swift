//
//  PatientTableViewCell.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 31/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class PatientTableViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPatientID: UILabel!
    @IBOutlet weak var labelPatientDetails: UILabel!
    @IBOutlet weak var patientImg: UIImageView!
    @IBOutlet weak var buttonOptions: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.patientImg.makeRoundedCorner()
    }
    
}


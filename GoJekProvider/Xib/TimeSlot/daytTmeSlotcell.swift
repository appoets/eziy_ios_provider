//
//  daytTmeSlotcell.swift
//  GoJekProvider
//
//  Created by Shyamala's MacBook Pro on 06/09/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import UIKit

class daytTmeSlotcell: UICollectionViewCell {
    
    @IBOutlet weak var daytimeView : UIView!
    @IBOutlet weak var daytimeLbl : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.daytimeView.cornerRadius = self.daytimeView.frame.width / 2
        self.daytimeView.layer.borderColor = UIColor.blue.cgColor
        self.daytimeView.layer.borderWidth = 1
        self.daytimeLbl.font = .setCustomFont(name: .bold, size: .x14)
       
    }

}

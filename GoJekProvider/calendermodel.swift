//
//  calendermodel.swift
//  GoJekProvider
//
//  Created by Nivedha's MacBook Pro on 30/03/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct calendermodel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : calenderResponseData?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct calenderResponseData : Mappable {
    var current_page : Int?
    var data : [String]?
    var first_page_url : String?
    var from : String?
    var last_page : Int?
    var last_page_url : String?
    var links : [Links]?
    var next_page_url : String?
    var path : String?
    var per_page : Int?
    var prev_page_url : String?
    var to : String?
    var total : Int?
    init(){}
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        current_page <- map["current_page"]
        data <- map["data"]
        first_page_url <- map["first_page_url"]
        from <- map["from"]
        last_page <- map["last_page"]
        last_page_url <- map["last_page_url"]
        links <- map["links"]
        next_page_url <- map["next_page_url"]
        path <- map["path"]
        per_page <- map["per_page"]
        prev_page_url <- map["prev_page_url"]
        to <- map["to"]
        total <- map["total"]
    }

}
struct Links : Mappable {
    var url : String?
    var label : String?
    var active : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        url <- map["url"]
        label <- map["label"]
        active <- map["active"]
    }

}



struct calenderAvailableModel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [Doctor_request]?
    var error : [String]?
    init(){}
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}


struct BlockListModel : Mappable {
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : [BlockDataModel]?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}

struct BlockDataModel : Mappable {
    var to_date : String?
    var type : String?
    var from_date : String?
    var reason : String?
    var doctor_request_id : String?
    var provider_id : Int?
    var id : Int?
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        to_date <- map["to_date"]
        type <- map["type"]
        from_date <- map["from_date"]
        reason <- map["reason"]
        doctor_request_id <- map["doctor_request_id"]
        provider_id <- map["provider_id"]
        id <- map["id"]
    }

}


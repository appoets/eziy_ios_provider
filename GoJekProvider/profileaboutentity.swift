//
//  profileaboutentity.swift
//  GoJekProvider
//
//  Created by Mareeswaran on 06/05/22.
//  Copyright © 2022 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

struct profileaboutentity : Mappable {
    
    var statusCode : String?
    var title : String?
    var message : String?
    var responseData : ResponseData?
    var error : [String]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        statusCode <- map["statusCode"]
        title <- map["title"]
        message <- map["message"]
        responseData <- map["responseData"]
        error <- map["error"]
    }

}
struct ResponseData : Mappable {
    var doctorProfile : DoctorProfile?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        doctorProfile <- map["doctorProfile"]
    }

}


struct DoctorProfile : Mappable {
    var provider_id : Int?
    var company_id : Int?
    var qualification : String?
    var experience : String?
    var specialist : String?
    var service : String?
    var specializations : String?
    var location : String?
    var dob : String?
    var status : String?
    var consultation_fee : String?
    var schedule_date : String?
    var picture : String?
    var medical_picture : String?
    var clinic_photos : String?
    var id : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        provider_id <- map["provider_id"]
        company_id <- map["company_id"]
        qualification <- map["qualification"]
        experience <- map["experience"]
        specialist <- map["specialist"]
        service <- map["service"]
        specializations <- map["specializations"]
        location <- map["location"]
        dob <- map["dob"]
        status <- map["status"]
        consultation_fee <- map["consultation_fee"]
        schedule_date <- map["schedule_date"]
        picture <- map["picture"]
        medical_picture <- map["medical_picture"]
        clinic_photos <- map["clinic_photos"]
        id <- map["id"]
    }

}

